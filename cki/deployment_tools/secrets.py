"""Implement secrets and variable access as used in the infrastructure repo."""
import argparse
import functools
import json
import os
import pathlib
import re
import subprocess
import sys
import typing

from cki_lib import misc
from cki_lib.logger import get_logger
from cki_lib.session import get_session
import hvac
import yaml

LOGGER = get_logger('cki.deployment_tools.secrets')
SESSION = get_session('cki.deployment_tools.secrets')


# pylint: disable=too-many-ancestors
class SecretsBlockDumper(yaml.SafeDumper):
    """Block-style literals."""

    def represent_str(self, data: str) -> yaml.ScalarNode:
        """Format some strings as block-style literals."""
        if '\n' not in data and not data.startswith('U2FsdGVkX1'):
            return super().represent_str(data)
        return self.represent_scalar('tag:yaml.org,2002:str', data, style='|')


@functools.lru_cache()
def _read_secrets_file(file_path: str) -> typing.Dict[str, typing.Any]:
    secrets = yaml.safe_load(pathlib.Path(file_path).read_text(encoding='utf8'))
    if not isinstance(secrets, dict):
        raise Exception('Invalid secrets file')
    return secrets


def _read_secrets_files(env_names: typing.Sequence[str]) -> typing.Any:
    result = {}
    for env_name in env_names:
        if file_name := os.environ.get(env_name):
            result.update(_read_secrets_file(file_name))
    return result


def encrypt(value: str, *, salt: bool = True) -> str:
    """Encrypt a secret."""
    process = subprocess.run([
        'openssl', 'enc',
        '-aes-256-cbc',
        '-md', 'sha512',
        '-pbkdf2',
        '-iter', '100000',
        '-salt' if salt else '-nosalt',
        '-pass', 'env:ENVPASSWORD',
        '-e', '-a'
    ], encoding='utf8', input=value, stdout=subprocess.PIPE, check=True)
    return process.stdout.strip()


def decrypt(value: str, *, salt: bool = True) -> str:
    """Decrypt a secret."""
    process = subprocess.run([
        'openssl', 'enc',
        '-aes-256-cbc',
        '-md', 'sha512',
        '-pbkdf2',
        '-iter', '100000',
        '-salt' if salt else '-nosalt',
        '-pass', 'env:ENVPASSWORD',
        '-d', '-a'
    ], encoding='utf8', input=value + '\n', stdout=subprocess.PIPE, check=True)
    return process.stdout.strip()


def secret(key: str) -> typing.Any:
    """Return a decrypted secret from the secrets file."""
    all_secrets = _read_secrets_files(['CKI_SECRETS_FILE'])

    path, sep, field = key.partition(':') if ':' in key else key.partition('#')

    if not (match := re.fullmatch(r'(?P<path>.*)\[(?P<conditions>.*)\]', path)):
        return _single_secret(all_secrets, path, sep, field)

    conditions = re.findall('(!?)([^,]+)', match['conditions'])
    return [
        _single_secret(all_secrets, k, sep, field)
        for k, v in all_secrets.items()
        if (k == match['path'] or k.startswith(f"{match['path']}/")) and
        all(v['.meta'].get(c[1], False) == (c[0] != '!') for c in conditions)
    ]


def _single_secret(
    all_secrets: typing.Dict[str, typing.Any],
    path: str,
    sep: str,
    field: str,
) -> typing.Any:
    if (value := all_secrets.get(path)) is None:
        raise Exception(f'Secret {path} not found in secrets files')
    if sep == '':
        sep, field = ':', 'value'
    # meta from yaml
    if sep == '#':
        return value['.meta'][field] if field else value.get('.meta', {})
    # secrets from yaml
    if (data := value.get('.data')) is not None:
        return decrypt(data[field]) if field else {k: decrypt(v) for k, v in data.items()}
    # secrets from HashiCorp Vault
    data = hvac.Client(session=SESSION).secrets.kv.read_secret(
        f'cki/{path}',
        mount_point='apps',
        raise_on_deleted_version=True
    )['data']['data']
    return data[field] if field else data


def edit(key: str, value: typing.Any) -> None:
    """Update a secret in a secrets file."""
    if not (secrets_file := os.environ.get('CKI_SECRETS_FILE')):
        raise Exception('CKI_SECRETS_FILE env variable missing')
    file_path = pathlib.Path(secrets_file)
    if file_path.exists():
        file_lines = file_path.read_text(encoding='utf8').strip().split('\n')
    else:
        LOGGER.warning('Creating new secrets file %s', secrets_file)
        file_lines = []
    path, sep, field = key.partition(':') if ':' in key else key.partition('#')
    line_start = next((i for i, l in enumerate(file_lines)
                       if l.startswith(f'{path}:')), len(file_lines))
    line_end = next((i for i, l in enumerate(file_lines[line_start + 1:], line_start + 1)
                     if l.strip() and not l.startswith(' ')), len(file_lines))
    if line_end and not file_lines[line_end - 1]:
        line_end -= 1

    data = (yaml.safe_load('\n'.join(file_lines[line_start:line_end])) or {}).get(path, {})
    match sep, field:
        case ':', '':
            data['.data'] = {
                k: encrypt(v) for k, v in
                (yaml.safe_load(value) if isinstance(value, str) else value).items()
            }
        case ':', _:
            data.setdefault('.data', {})[field] = encrypt(value)
        case '#', '':
            data['.meta'] = yaml.safe_load(value) if isinstance(value, str) else value
        case '#', _:
            data.setdefault('.meta', {})[field] = value
        case '', _:
            data.setdefault('.data', {})['value'] = encrypt(value)
    new_lines = yaml.dump({path: data}, Dumper=SecretsBlockDumper).strip().split('\n')
    lines = file_lines[0:line_start] + new_lines + file_lines[line_end:]
    file_path.write_text('\n'.join(lines) + '\n', encoding='utf8')
    _read_secrets_file.cache_clear()


def variable(key: str) -> typing.Any:
    """Return an unencrypted variable from the variables files."""
    if (value := _read_secrets_files(
            os.environ.get('CKI_VARS_NAMES', '').split() + ['CKI_VARS_FILE']).get(key)) is None:
        raise Exception(f'Variable {key} not found in variable files')
    return value.strip() if isinstance(value, str) else value


def _encrypt_cli(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Encrypt a secret via the CLI."""
    _main(['encrypt'] + (argv or sys.argv[1:]))


def _secret_cli(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Return a decrypted secret from the secrets file via the CLI."""
    _main(['secret'] + (argv or sys.argv[1:]))


def _variable_cli(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Return an unencrypted variable from the variables files via the CLI."""
    _main(['variable'] + (argv or sys.argv[1:]))


def _edit_cli(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Edit a secret in the secrets file via the CLI."""
    _main(['edit'] + (argv or sys.argv[1:]))


def _main(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Access the CKI secrets tools."""
    parser = argparse.ArgumentParser(description='Access CKI variables and secrets')
    parser.add_argument('--json', action='store_true', help='output in json format')
    parser.add_argument('type', choices=('encrypt', 'secret', 'variable', 'edit'))
    parser.add_argument('data', nargs='+')
    args = parser.parse_args(argv)

    if (result := globals()[args.type](*args.data)) is not None:
        if args.json:
            print(json.dumps(result))
        else:
            for single_result in misc.flattened(result):
                print(single_result)


yaml.add_representer(str, SecretsBlockDumper.represent_str, Dumper=SecretsBlockDumper)

if __name__ == '__main__':
    _main()
