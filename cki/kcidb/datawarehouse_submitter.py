"""Submit GitLab results to DataWarehouse."""
import argparse
from http import HTTPStatus
import json
import os
import typing

from cki_lib import gitlab
from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from cki_lib.kcidb import ValidationError
from cki_lib.kcidb import validate_extended_kcidb_schema
from cki_lib.logger import get_logger
import datawarehouse
from gitlab.exceptions import GitlabGetError
from requests.exceptions import HTTPError
import sentry_sdk

LOGGER = get_logger('cki.kcidb.datawarehouse_submitter')


def callback(body=None, **_) -> None:
    """Process a single GitLab job."""
    LOGGER.info('Processing %s', body['web_url'])
    gl_instance, gl_job = gitlab.parse_gitlab_url(body['web_url'])
    with gl_instance:
        try:
            kcidb_all_content = gl_job.artifact('kcidb_all.json')
        except GitlabGetError:
            LOGGER.info('Job %s has no kcidb_all.json file', gl_job.name)
            return

    if not kcidb_all_content:
        LOGGER.info('Job %s has an empty kcidb_all.json file', gl_job.name)
        return

    try:
        kcidb_data = json.loads(kcidb_all_content)
    except json.JSONDecodeError as error:
        LOGGER.info('Job %s has an invalid kcidb_all.json file: %s', gl_job.name, error)
        return

    try:
        validate_extended_kcidb_schema(kcidb_data)
    except ValidationError:
        LOGGER.exception('Job %s has an invalid kcidb_all.json file.', gl_job.name)
        return

    if misc.is_production_or_staging():
        dw_api = datawarehouse.Datawarehouse(os.environ['DATAWAREHOUSE_URL'],
                                             os.environ['DATAWAREHOUSE_TOKEN_SUBMITTER'])

        try:
            dw_api.kcidb.submit.create(data=kcidb_data)
        except HTTPError as error:
            if error.response.status_code == HTTPStatus.BAD_REQUEST:
                LOGGER.exception("Failed to submit data to DataWarehouse")
            else:
                raise
    else:
        LOGGER.info('Would submit %s if in production or staging', kcidb_data)


def main(argv: typing.Optional[typing.List[str]] = None):
    """Submit GitLab results to DataWarehouse."""
    misc.sentry_init(sentry_sdk)
    parser = argparse.ArgumentParser()
    parser.add_argument('job_url', nargs='?', help='Upload data from a single GitLab job')
    args = parser.parse_args(argv)

    if args.job_url:
        callback(body={'web_url': args.job_url})
    else:
        metrics.prometheus_init()
        messagequeue.MessageQueue().consume_messages(
            os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.exchange.webhooks'),
            os.environ['DATAWAREHOUSE_SUBMITTER_ROUTING_KEYS'].split(),
            callback,
            queue_name=os.environ.get('DATAWAREHOUSE_SUBMITTER_QUEUE'),
        )


if __name__ == '__main__':
    main()
