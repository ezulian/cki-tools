"""Listen for UMB messages and submit them to Datawarehouse."""
from http import HTTPStatus
import os

from cki_lib import logger
from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from cki_lib.kcidb import ValidationError
from cki_lib.kcidb import validate_extended_kcidb_schema
import datawarehouse
import prometheus_client as prometheus
from requests.exceptions import HTTPError
import sentry_sdk

LOGGER = logger.get_logger('cki.kcidb.datawarehouse_umb_submitter')


METRIC_UMB_RESULTS = prometheus.Counter(
    'umb_results', 'Number of UMB results submitted to datawarehouse')


class Receiver:
    # pylint: disable=too-few-public-methods
    """Provides callback to submit to Datawarehouse."""

    def __init__(self):
        """Initialize Datawarehouse object used for every callback."""
        self.dw_api = datawarehouse.Datawarehouse(os.environ['DATAWAREHOUSE_URL'],
                                                  os.environ['DATAWAREHOUSE_TOKEN_SUBMITTER'])

    def callback(self, body, **_):
        """Submit a UMB result message to datawarehouse."""
        try:
            validate_extended_kcidb_schema(body)
        except ValidationError:
            LOGGER.exception("Failed to validate data from UMB")
            return

        try:
            self.dw_api.kcidb.submit.create(data=body)
        except HTTPError as error:
            if error.response.status_code == HTTPStatus.BAD_REQUEST:
                LOGGER.exception("Failed to submit data from UMB to DataWarehouse")
            else:
                raise
        else:
            METRIC_UMB_RESULTS.inc()


def main():
    """Submit UMB results to datawarehouse."""
    misc.sentry_init(sentry_sdk)
    metrics.prometheus_init()
    receiver = Receiver()

    connection = messagequeue.MessageQueue()
    connection.consume_messages(
        os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.exchange.webhooks'),
        os.environ['RABBITMQ_ROUTING_KEYS'].split(),
        receiver.callback,
        queue_name=os.environ['RABBITMQ_QUEUE']
    )


if __name__ == '__main__':
    main()
