"""Misc utility functions for kcidb."""
import hashlib
import re

from cki_lib.session import get_session

SESSION = get_session(__name__)


def patch_list_hash(patch_list):
    """Get hash of a list of patches."""
    if not patch_list:
        return ''

    patch_hash_list = []
    for patch_url in patch_list:
        # Transform /mbox/ url into /raw/ to get the patch diff only.
        # Patchwork mbox includes headers that can change after people reply to the patches.
        patch = SESSION.get(re.sub(r'/mbox/?$', '/raw/', patch_url))
        patch_hash_list.append(
            hashlib.sha256(patch.content).hexdigest()
        )

    merged_hashes = '\n'.join(patch_hash_list) + '\n'
    return hashlib.sha256(merged_hashes.encode('utf8')).hexdigest()
