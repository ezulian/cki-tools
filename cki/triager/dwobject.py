"""DataWarehouse object wrapper."""

from . import settings


def get_manager_for_obj_type(obj_type: str):
    """Map obj_type to DW API managers."""
    return {
        'checkout': settings.DW_CLIENT.kcidb.checkouts,
        'build': settings.DW_CLIENT.kcidb.builds,
        'test': settings.DW_CLIENT.kcidb.tests,
        'testresult': settings.DW_CLIENT.kcidb.testresults,
    }[obj_type]


def from_obj_id(obj_type: str, obj_id):
    """Fetch a DW object of a given type, and add a type field to it."""
    obj = get_manager_for_obj_type(obj_type).get(obj_id)
    obj.type = obj_type
    return obj


def from_attrs(obj_type: str, attrs):
    """Initialize a DW object of a given type, and add a type field to it."""
    manager = get_manager_for_obj_type(obj_type)
    # pylint: disable=protected-access
    obj = manager._obj_cls(manager=manager, attrs=attrs)
    obj.type = obj_type
    return obj
