"""Install setup.cfg dependencies via dnf."""
import argparse
import collections
import configparser
import http
import io
import pathlib
import re
import subprocess
import typing
import urllib.request

Dependency = collections.namedtuple('Dependency', 'name version extras_string url')


class DnfResolver:
    """Lookup Python package dependencies via dnf."""

    package_url_pattern = re.compile(r'''
        ([-_.A-Za-z0-9]+)                       # name
        (?:\[([a-z_]+)\])?                      # extras
        \s*@\s*
        (
            git\+https://[^@<>= ]+              # url
            (@[^@<>= ]*)?                       # ref
        )
        ''', re.X)

    package_pattern = re.compile(r'''
        ([-_.A-Za-z0-9]+)                       # name
        (?:\[([a-z_,]+)\])?                     # extras
        \s*
        ((?:<|<=|==|>=|>)\s*[-_.A-Za-z0-9]+)?   # version
        ''', re.X)

    def __init__(
            self,
            recursive: bool,
    ):
        """Create a new instance."""
        self.recursive = recursive
        self.pip_via_distribution: typing.Dict[Dependency, str] = {}
        self.pip_unavailable: typing.Set[Dependency] = set()

    @staticmethod
    def _clean_value(
            parser: configparser.ConfigParser,
            section: str,
            key: str
    ) -> typing.Set[str]:
        return {p.split('#', 1)[0].strip()
                for p in parser.get(section, key, fallback='').strip().split('\n')
                if p}

    @staticmethod
    def _parse_dependency(line: str) -> Dependency:
        if 'git+https' in line:
            parts = DnfResolver.package_url_pattern.fullmatch(line)
            if not parts:
                raise Exception(f'Unable to parse dependency: {line}')
            name = parts.group(1)
            version: typing.Optional[str] = None
            extras_string = parts.group(2) or ''
            url: typing.Optional[str] = parts.group(3)
        else:
            parts = DnfResolver.package_pattern.fullmatch(line)
            if not parts:
                raise Exception(f'Unable to parse dependency: {line}')
            name = parts.group(1)
            version = parts.group(3)
            extras_string = parts.group(2) or ''
            url = None
        name = re.sub('[^a-z0-9.]', '-', name.lower())
        return Dependency(name, version, extras_string, url)

    @staticmethod
    def _map_dependency_package(dependency: Dependency) -> str:
        overrides = {
            'psycopg2-binary': 'python3-psycopg2',
            'cli-proton-python': 'python3-qpid-proton',
            # not available, blinker is the only indirect dependency in the repos
            'sentry-sdk+flask': 'python3-blinker',
        }
        if dependency.extras_string:
            name = f'{dependency.name}+{"+".join(sorted(dependency.extras_string.split(",")))}'
            default = fr'python3dist({dependency.name}\[{dependency.extras_string}\])'
        else:
            name = dependency.name
            default = f'python3dist({dependency.name})'
        return overrides.get(name, default)

    @staticmethod
    def _package_versions(package: str) -> typing.List[str]:
        process = subprocess.run(['dnf', 'repoquery', '--whatprovides',
                                  package], check=True, capture_output=True, encoding='utf8')
        return process.stdout.strip().split()

    @staticmethod
    def _url(url: str) -> typing.Optional[str]:
        try:
            response: http.client.HTTPResponse
            with urllib.request.urlopen(url) as response:
                return response.read().decode('utf8')
        except urllib.error.HTTPError as error:
            if error.code == 404:
                return None
            raise

    @staticmethod
    def _setup_cfg_contents(url_prefix: str) -> typing.Optional[str]:
        result = DnfResolver._url(f'{url_prefix}/setup.cfg')
        if result:
            return result
        # as an awful hack, try regex magic on setup.py and see what sticks
        setup_py = DnfResolver._url(f'{url_prefix}/setup.py')
        if setup_py:
            install_requires = re.search(
                r'install_requires\s*=\s*\[((?:\s*"[^"]*"\s*,?)+)\s*]', setup_py)
            if install_requires:
                packages = [p.strip(' \n"') for p in install_requires.group(1).split(',') if p]
                config = configparser.ConfigParser()
                config.add_section('options')
                config['options']['install_requires'] = '\n'.join(packages)
                buf = io.StringIO()
                config.write(buf)
                result = buf.getvalue()
        return result

    def resolve_dependency(self, dependency: Dependency) -> None:
        """Try to resolve Python package dependencies via dnf."""
        if dependency.url.startswith('git+https://'):
            parts = dependency.url[4:].split('@', 1)
            if len(parts) > 1:
                url, ref = parts
            else:
                url, ref = parts[0], 'HEAD'
        else:
            raise Exception(f'Unsupported protocol for {dependency.url}')
        url = re.sub('(.git)?/?$', '', url)
        if url.startswith('https://gitlab.com'):
            url_prefix = f'{url}/-/raw/{ref}'
        elif url.startswith('https://github.com'):
            url_prefix = f'https://raw.githubusercontent.com/{url[19:]}/{ref}'
        else:
            raise Exception(f'Unsupported git forge for {dependency.url}')
        setup_cfg_contents = self._setup_cfg_contents(url_prefix)
        if setup_cfg_contents:
            self.resolve(setup_cfg_contents, dependency.extras_string.split(','), [])

    def resolve(
            self,
            setup_cfg_contents: str,
            extras: typing.List[str],
            environments: typing.List[str],
    ) -> None:
        """Try to resolve Python package dependencies via dnf."""
        parser = configparser.ConfigParser()
        parser.read_string(setup_cfg_contents)

        packages = self._clean_value(parser, 'options', 'install_requires')
        for extra in extras:
            packages |= self._clean_value(parser, 'options.extras_require', extra)
        for environment in environments:
            packages |= self._clean_value(parser, environment, 'deps')
            for extra in self._clean_value(parser, environment, 'extras'):
                packages |= self._clean_value(parser, 'options.extras_require', extra)
        for line in packages:
            dependency = self._parse_dependency(line)
            if (dependency in self.pip_unavailable) or (dependency in self.pip_via_distribution):
                continue
            if dependency.url:
                self.pip_unavailable.add(dependency)
                if self.recursive:
                    print(f'Resolving recursive dependencies of "{line}"')
                    self.resolve_dependency(dependency)
                else:
                    print(f'Ignoring URL-based "{line}"')
                continue
            # if dependency.version:
            #    print(f'Ignoring versioned "{line}"')
            #    self.pip_unavailable.add(dependency)
            #    continue
            print(f'Resolving dependency "{line}"')
            package_candidate = self._map_dependency_package(dependency)
            versions = self._package_versions(package_candidate)
            if versions:
                self.pip_via_distribution[dependency] = package_candidate
            else:
                self.pip_unavailable.add(dependency)

    def status(self) -> None:
        """Print information about package sources."""
        if self.pip_via_distribution:
            print('Installation via dnf:')
            for dependency in self.pip_via_distribution:
                print(f'  {dependency.name}')
        if self.pip_unavailable:
            print('Installation via pip:')
            for dependency in self.pip_unavailable:
                print(f'  {dependency.name}')

    def install_via_distribution(self) -> None:
        """Install Python package dependencies via dnf."""
        subprocess.run(['dnf', 'install', '--assumeyes', '--'] +
                       list(self.pip_via_distribution.values()), check=True)


def main(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Run the command line interface."""
    parser = argparse.ArgumentParser()
    parser.add_argument('config', nargs='*', default=['setup.cfg'],
                        help='Config file path')
    parser.add_argument('--extra', default=[], action='append',
                        help='options.extras_require to add to the package set')
    parser.add_argument('--environment', default=[], action='append',
                        help='environment.extras to add to the package set')
    parser.add_argument('--dry-run', action='store_true',
                        help='Do not actually install packages')
    parser.add_argument('--recursive', action='store_true',
                        help='Resolve dependencies of git+https URLs')
    args = parser.parse_args(argv)

    resolver = DnfResolver(recursive=args.recursive)
    for config in args.config:
        resolver.resolve(pathlib.Path(config).read_text(encoding='utf8'),
                         args.extra, args.environment)

    if args.dry_run:
        resolver.status()
    else:
        resolver.install_via_distribution()


if __name__ == '__main__':
    main()
