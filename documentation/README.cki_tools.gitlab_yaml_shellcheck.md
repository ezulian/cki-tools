---
title: cki_tools.gitlab_yaml_shellcheck
linkTitle: gitlab_yaml_shellcheck
description: >-
    Runs `shellcheck` on the shell code in the different jobs for a GitLab
    CI/CD pipeline `gitlab-ci.yml`
---

```shell
usage: python3 -m cki_tools.gitlab_yaml_shellcheck [-h]
  [--job JOB]
  [--check-sourced]
  [--stdin-filename STDIN_FILENAME]
  [--verbose]
  [--no-cache]
  file

positional arguments:
  file                  GitLab CI/CD pipeline file

options:
  -h, --help            show this help message and exit
  --job JOB             Only lint the given jobs
  --check-sourced       show warnings from followed "source" commands
  --stdin-filename STDIN_FILENAME
                        Use the given file name for the contents of standard input
  --verbose             provide progress information
  --no-cache            do not cache shellcheck output between calls
```

By default, all jobs are checked. The shell scripts for `before_script`,
`script` and `after_script` are extracted and combined with the corresponding
variables into a temporary shell script, which is then checked by `shellcheck`.
When `--checked-sourced` is specified, messages for sourced scripts are shown
as well. Any unknown arguments are passed to shellcheck.

Without `--checked-sourced`, an offline cache with `shellcheck` output in
`~/.cache/gitlab-yaml-shellcheck` is used to try to avoid checking unmodified
jobs. The cache key depends on the script contents, line numbers and shellcheck
options. The cache will not be invalidated if any of the sourced scripts
change. If this is required, use `--no-cache`.

For [ALE/Vim](https://github.com/dense-analysis/ale), an ALE linter definition
for pipeline YAMLs is provided in `vim/ale_linters`. To use it, add the `vim`
directory to your VIM runtime path via

```text
set runtimepath+=/path/to/cki-tools/vim
```
