---
title: cki_tools_kernel_config_updater.sh
description: >-
  Get newest Fedora Rawhide kernel configuration files to use for upstream
  kernel builds
aliases: [/l/kernel-config-updater]
---

```shell
cki_tools_kernel_config_updater.sh
```

The script downloads the kernel-core RPMs for Fedora Rawhide, and uploads the
extracted configs to the specified S3 bucket.

More information can be found in the [detailed documentation].

## Environment variables

| Name                      | Secret | Required | Description                                                           |
|---------------------------|--------|----------|-----------------------------------------------------------------------|
| `BUCKET_CONFIG_NAME`      | no     | yes      | name of environment variable with the bucket specification            |
| `BUCKET_CONFIG`           | yes    | yes      | bucket specification as configured in `BUCKET_CONFIG_NAME`            |

[detailed documentation]: /l/upstream-kernel-configs
