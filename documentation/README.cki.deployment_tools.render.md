---
title: cki.deployment_tools.render
linkTitle: render
description: Render text files via `jinja2`
---

```text
$ python3 -m cki.deployment_tools.render --help
usage: render.py [-h] [--package PACKAGE] [--data NAME=PATH] [--raw-data NAME=PATH]
                 [--arg NAME=VALUE] [--include-path INCLUDE_PATH] [--output OUTPUT]
                 [template]

Render templates

positional arguments:
  template              template file to render

optional arguments:
  -h, --help            show this help message and exit
  --package PACKAGE     Get the template from a package instead of from the file system
  --data NAME=PATH      JSON/YAML data file(s) to expose as NAME
  --raw-data NAME=PATH  raw data file(s) to expose as NAME
  --arg NAME=VALUE      variable value to expose as NAME
  --include-path INCLUDE_PATH
                        path for template includes
  --output OUTPUT       Output path for the rendered file.
```

The template will be read from stdin (`template` empty), from a file
(`template` specifies the file path), or from a file in a Python package
(`template` specifies the file name, `package` specifies the Python package).

Variable values passed via `--arg` will be exposed as strings.

`{{ name.data }}`. For `--data`, the file contents of yml/yaml/json files will
The data files will be exposed as variables that can be used via something like
`{{ name.data }}`. For `--data`, the file contents of yml/yaml/json files will
be parsed. For `--raw-data`, the raw text contents is provided. If a file name
ends in `.j2`, the file contents will be treated as a template.

As an example, the following files are present:

```yaml
# data-file.yml
foo: bar1

# data-dir/file1.yml
foo: bar2

# data-dir/dir1/file2.yml
foo: bar3

# raw-data.txt
bar4

# template.txt
{{ data1.foo }}
{{ data2.file1.foo }}
{{ data2.dir1.file2.foo }}
{{ data3["raw-data.txt"] }}
```

The template can be rendered via

```shell
python3 -m cki.deployment_tools.render \
    template.txt \
    --data data1=data-file.yml \
    --data data2=data-dir \
    --raw-data data3=raw-data.txt
```

The resulting output will be

```plain
bar1
bar2
bar3
bar4
```

Besides normal jinja2 templating, several custom globals/filters are provided:

- The `env` filter allows to replace variables following the bash notation
  (`${VAR_NAME}` or `$VAR_NAME`) with environment variables.

  If a variable is defined on the data file as `foo: ${BAR}`, and it's used
  with the `env` filter `{{ data.foo | env }}` it will be replaced with the
  content of the `BAR` environment variable. If `BAR` is not defined, a
  `KeyError` exception will be raised.

- The `is_true` filter converts a string to a boolean.

- The `from_yaml` filter converts a YAML/JSON string to an object.

- The `env` global variable exposes `os.environ` to the template.

- The `is_production` global variable exposes the result of `cki_lib.misc.is_production`.

- The `deployment_environment` global variable exposes the result of
  `cki_lib.misc.deployment_environment`.

- The `url(binary=False, json=False)` global function allows to call into
  `requests` to retrieve an external file.

- The `cki_variable(key)` global function allows to load deployment variables
  as used in the infrastructure repositories.

- The `cki_secret(key)` global function allows to load deployment secrets
  as used in the infrastructure repositories.
