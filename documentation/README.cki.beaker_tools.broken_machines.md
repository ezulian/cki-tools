---
title: cki.beaker_tools.broken_machines
linkTitle: broken_machines
description: Beaker broken machines updater
---

This script queries TEIID database and gets a list of broken
machines from [Beaker](https://beaker-project.org).

The produced list of machines is used when submitting testing
jobs so we avoid infrastructure failures which we'd encounter
by running on these machines.

| Environment variable          | Required | Default                     | Description                                                                |
|-------------------------------|----------|-----------------------------|----------------------------------------------------------------------------|
| `TEIID_HOST`                  | Yes      |                             | Hostname of TEIID instance                                                 |
| `TEIID_PORT`                  | Yes      |                             | Port of TEIID instance                                                     |
| `TEIID_DATABASE`              | Yes      |                             | Database name                                                              |
| `BUCKET_CONFIG_NAME`          | Yes      |                             | Name of environment variable with S3 bucket spec to use as a backing store |
| `CKI_DEPLOYMENT_ENVIRONMENT`  | No       | `staging`                   | Define the deployment environment (production/staging)                     |
| `SENTRY_SDN`                  | No       |                             | Sentry SDN                                                                 |
| `LIST_PATH`                   | No       | `/broken-machines-list.txt` | Path for the machines list file in the bucket                              |
| `THRESHOLD_RECIPES_RUN`       | No       | `15`                        | Minimum recipes a machine needs to have run to be included                 |
| `THRESHOLD_BROKEN`            | No       | `0.5`                       | Ratio of failed jobs for machines to be considered broken                  |
| `MIN_DAYS_TO_CHECK`           | No       | `3`                         | Minimum days in the past to be included in the checks                      |
| `MAX_DAYS_TO_CHECK`           | No       | `7`                         | Maximum days in the past to be included in the checks                      |
| `LIMIT_RESULTS`               | No       | `50`                        | Number of machines to be included in the resulting list                    |

## CKI_DEPLOYMENT_ENVIRONMENT

On staging developments (`CKI_DEPLOYMENT_ENVIRONMENT != production`), the
changes are only logged but not uploaded to the S3 bucket.
