---
title: cki.deployment_tools.secrets
linkTitle: secrets
description: Access CKI secrets
aliases: [/l/secrets-helper-docs]
---

The `secrets` tool allows access to CKI variables and secrets.

```text
$ python3 -m cki.deployment_tools.secrets --help
usage: secrets.py [-h] [--json] {encrypt,secret,variable} value

Access CKI variables and secrets

positional arguments:
  {secret,variable,edit} data [data ...]
  data

optional arguments:
  -h, --help            show this help message and exit
  --json                output in json format
```

The following CLI aliases are provided:

- reading secrets: `cki_secret` == `python3 -m cki.deployment_tools.secrets secret`
- reading variables: `cki_variable` == `python3 -m cki.deployment_tools.secrets variable`
- editing secrets: `cki_edit_secret` == `python3 -m cki.deployment_tools.secrets edit`

CKI variables and secrets meta data are stored in YAML files. Encrypted secrets
are stored in the YAML files as well, with a fallback to HashiCorp Vault if the
`VAULT_ADDR` environment variable is defined. This is expected to become the
default in the future.

## Secrets

For secrets, the file name needs to be specified in the `CKI_SECRETS_FILE`
environment variable. In the secrets file, secret values are encrypted via
OpenSSL. The encryption key needs to be provided in the `ENVPASSWORD`
environment variable.

As an example, with `ENVPASSWORD=password`, a secrets file could look like:

```yaml
foo:
  .data:
    value: |-
      U2FsdGVkX1/j2VLVqN+2VdOgGi6KZcUWq3hmFZkxF20=
```

Pointing `CKI_SECRETS_FILE` to it and calling `cki_secret foo` will print the
decrypted value of the `foo` key:

```shell
$ cki_secret foo
bar
$ cki_secret foo --json
"bar"
```

Secrets can be edited as well, and the code tries to preserve the structure of
the YAML file:

```yaml
# a lot of secrets

# this is foobar
foo:
  .data:
    value: |-
      U2FsdGVkX1/j2VLVqN+2VdOgGi6KZcUWq3hmFZkxF20=

# more to come later
```

```shell
$ cki_secret foo
bar
$ cki_edit_secret foo baz
$ cki_secret foo
baz
```

```yaml
# a lot of secrets

# this is foobar
foo:
  .data:
    value: |-
      U2FsdGVkX19Cl/zsFvomHG+i3dGuuRWIu6anvBOX3OY=

# more to come later
```

### Conditions

Conditions allow to obtain a list of meta/data fields for multiple secrets via
something like `path[meta-key-1,!meta-key-2,...]`. For all secrets that start
with the given path, delimited by `/`, the given meta fields are obtained with
a default of `False`. A secret is only included in the returned list if all
conditions match, i.e. if they are `True` for a condition like `meta-key-1`, or
`False` for a condition like `!meta-key-2`.

### Supported locations

| Location                 | Description                                |
|--------------------------|--------------------------------------------|
| `some/path#`             | dictionary of all key-value meta pairs     |
| `some/path#field`        | value for the given meta field             |
| `some/path:`             | dictionary of all key-value data pairs     |
| `some/path:field`        | value for the given data field             |
| `some/path`              | value for the `value` data field           |
| `some/path[cond1,cond2]` | list of the above for all matching secrets |

## Variables

For variables, the file name needs to be specified in the `CKI_VARS_FILE`
environment variable. A list of space-delimited additional environment variable
names can be provided in the `CKI_VARS_NAMES` environment variable.

As an example, a variables file could look like:

```yaml
foo: bar
baz: |
  some
  string
qux:
  complex: value
bool: true
int: 15
```

Pointing `CKI_VARS_FILE` to it and calling `cki_variable key` will print the
values of the various variables:

```shell
$ for i in foo baz qux bool int; do cki_variable $i; done
bar
some
string
{'complex': 'value'}
True
15
```

With `--json`, the output will be properly json-encoded:

```shell
$ for i in foo baz qux bool int; do cki_variable --json $i; done
"bar"
"some\nstring"
{"complex": "value"}
true
15
```
