---
title: cki.cki_tools.install_dependencies
linkTitle: install_dependencies
description: Try to install as many Python dependencies as possible via dnf
---

```shell
Usage: python3 -m cki.cki_tools.install_dependencies
    [--extra EXTRA]
    [--environment ENVIRONMENT]
    [--dry-run]
    [--recursive]
    [config [config ...]]
```

The `setup.cfg` files specified on the command line will be parsed for
dependencies, taking the given extras and environments into account. If
requested, packages provided via git+https are inspected recursively for
dependencies as well. With `--dry-run`, packages are not actually installed.
