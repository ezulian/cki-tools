---
title: cki_tools_git_cache_updater.sh
description: Update a cache of tar files containing git repositories in S3
---

```shell
cki_tools_git_cache_updater.sh
```

## Environment variables

| Name                      | Secret | Required | Description                                                           |
|---------------------------|--------|----------|-----------------------------------------------------------------------|
| `BUCKET_CONFIG_NAME`      | no     | yes      | name of environment variable with the bucket specification            |
| `BUCKET_CONFIG`           | yes    | yes      | bucket specification as configured in `BUCKET_CONFIG_NAME`            |
| `REPOS`                   | no     | yes      | space-separated list of git repositories to cache                     |
| `GITLAB_READ_REPO_TOKENS` | no     | no       | URL/environment variable pairs of GitLab instances and private tokens |
| `GITLAB_TOKEN`            | yes    | no       | GitLab private tokens as configured in `gitlab_tokens` above          |
