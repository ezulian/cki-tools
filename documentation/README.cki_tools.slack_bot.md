---
title: cki_tools.slack_bot
linkTitle: slack_bot
description: Slack bot hooked up to print pipeline status and notifications
---

## Configuration via environment variables

| Name                         | Secret | Required | Description                                                                            |
|------------------------------|--------|----------|----------------------------------------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT` | no     | no       | Define the deployment environment (production/staging)                                 |
| `GITLAB_TOKENS`              | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens as a JSON object |
| `GITLAB_TOKEN`               | yes    | yes      | GitLab private tokens as configured in `gitlab_tokens` above                           |
| `CKI_METRICS_ENABLED`        | no     | no       | Enable prometheus metrics. Default: false                                              |
| `CKI_METRICS_PORT`           | no     | no       | Port where prometheus metrics are exposed. Default: 8000                               |
| `CKI_SLACK_BOT_WEBHOOK`      | yes    | yes      | Slack Webhook URL                                                                      |
| `RABBITMQ_HOST`              | no     | yes      | AMQP host                                                                              |
| `RABBITMQ_PORT`              | no     | yes      | AMQP port, TLS is used for port 443                                                    |
| `RABBITMQ_USER`              | no     | yes      | AMQP user                                                                              |
| `RABBITMQ_PASSWORD`          | yes    | yes      | AMQP password                                                                          |
| `RABBITMQ_CAFILE`            | no     | yes      | AMQP CA file path                                                                      |
| `RABBITMQ_CERTFILE`          | no     | yes      | AMQP certificate + private key file path                                               |
| `WEBHOOK_RECEIVER_EXCHANGE`  | no     | yes      | AMQP exchange to receive messages                                                      |
| `SLACK_BOT_QUEUE`            | no     | yes      | AMQP queue name that gets hooked up to the exchange                                    |
| `SLACK_BOT_ROUTING_KEYS`     | no     | yes      | AMQP routing keys for the messages sent to the queue                                   |
