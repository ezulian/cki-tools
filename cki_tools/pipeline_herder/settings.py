"""Settings read from env variables."""
import os

from cki_lib import misc

HERDER_ACTION = os.environ.get('HERDER_ACTION', 'report')
HERDER_RETRY_LIMIT = misc.get_env_int('HERDER_RETRY_LIMIT', 3)
HERDER_RETRY_DELAYS = [int(d) for d in
                       os.environ.get('HERDER_RETRY_DELAYS', '0,5,10').split(',')]
HERDER_MAXIMUM_ARTIFACT_SIZE = misc.get_env_int('HERDER_MAXIMUM_ARTIFACT_SIZE', 100_000_000)

RABBITMQ_PUBLISH_EXCHANGE = os.environ.get('PIPELINE_HERDER_PUBLISH_EXCHANGE')

RABBITMQ_WEBHOOKS_EXCHANGE = os.environ.get(
    'WEBHOOK_RECEIVER_EXCHANGE', 'cki.exchange.webhooks')
RABBITMQ_WEBHOOKS_ROUTING_KEYS = os.environ.get('PIPELINE_HERDER_ROUTING_KEYS', '').split()
RABBITMQ_WEBHOOKS_QUEUE = os.environ.get('PIPELINE_HERDER_QUEUE')
