"""OSCI gating reporter."""
import argparse
import base64
import datetime
import gzip
import itertools
import os
import re
import typing
import xml.etree.ElementTree as ET

from cki_lib import logger
from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from cki_lib import session
from cki_lib import stomp
from cki_lib import yaml
import datawarehouse
import sentry_sdk

LOGGER = logger.get_logger('cki_tools.gating_reporter')
SESSION = session.get_session('cki_tools.gating_reporter')

SUPPORTED_KOJI_INSTANCES = yaml.load(contents=os.environ.get('SUPPORTED_KOJI_INSTANCES', '[]'))


def compress_and_b64encode(data: str) -> str:
    """Compress and perform base64 encoding on the input data."""
    encoded_data = data.encode('utf-8')
    return base64.b64encode(gzip.compress(encoded_data)).decode('utf-8')


def _dict_to_xml(dictionary: typing.Mapping[str, typing.Any], parent: ET.Element) -> None:
    for key, value in dictionary.items():
        if isinstance(value, dict):
            element = ET.Element(key)
            parent.append(element)
            _dict_to_xml(value, element)
        elif isinstance(value, list):
            element = ET.Element(key)
            parent.append(element)
            for item in value:
                _dict_to_xml(item, element)
        elif key.startswith("@"):
            parent.set(key[1:], str(value))
        else:
            element = ET.SubElement(parent, key)
            element.text = str(value)


def dict_to_xml(dictionary: typing.Mapping[str, typing.Any]) -> str:
    """
    Convert a dictionary representation of xunit into its corresponding XML format.

    Args:
        dictionary (dict): The dictionary representing xunit data.

    Returns:
        str: The XML string representation of the xunit data.
    """
    # The dummy tags are used to construct the XML structure and are removed at the end.
    root = ET.Element('dummy_root')
    _dict_to_xml(dictionary, root)
    # Create a string buffer to hold the XML content
    xml_string = ET.tostring(root, encoding='utf-8', method='xml')
    # Insert the XML declaration at the beginning of the string
    xml_declaration = b'<?xml version="1.0" encoding="utf-8"?>\n'
    xml_string_with_declaration = xml_declaration + xml_string
    # Remove dummy tags from the result
    return str(re.sub(r'</?dummy_(root|testsuites|testcases)\s?/?>',
                      '',
                      xml_string_with_declaration.decode())
               )


def _make_xunit_dict(test):
    testcases = []
    subtests = test.misc.get('results', [])
    xunit_dict = {
        'testsuite': {
            '@name': test.comment,
            '@result': _status_to_osci_dashboard_format(test.status),
            '@tests': str(len(subtests)),
            'properties': {
                'property': {
                    '@name': 'baseosci.result',
                    '@value': _status_to_osci_dashboard_format(test.status)
                }
            },
            'dummy_testcases': []
        }
    }
    for subtest in subtests:
        testcase = {
            'testcase': {
                '@name': subtest['name'],
                '@result': _status_to_osci_dashboard_format(subtest['status']),
                'logs': [],
            }
        }
        if subtest['status'] == 'FAIL':
            testcase['testcase']['failure'] = {}
        for output_file in subtest['output_files']:
            testcase['testcase']['logs'].append(
                {
                    'log': {
                        '@href': output_file['url'],
                        '@name': output_file['name']
                    }

                }
            )
        testcases.append(testcase)
    xunit_dict['testsuite']['dummy_testcases'] = testcases
    return xunit_dict


def _status_to_osci_dashboard_format(status: str) -> str:
    """Convert status to the format understood by the OSCI dashboard."""
    if status == 'FAIL':
        return 'failed'
    return 'passed'


def make_xunit_dict(tests):
    """
    Generate an xunit dictionary representation for a list of KCIDBTest objects.

    Args:
        tests (list): A list of KCIDBTest objects representing various test results.

    Returns:
        dict: The xunit dictionary representation containing the test results.

    """
    overall_result = _status_to_osci_dashboard_format(
        'FAIL' if any(test.status == 'FAIL' for test in tests) else 'PASS'
    )
    results = [_make_xunit_dict(test) for test in tests]

    xunit_dict = {
        'testsuites': {
            '@overall-result': overall_result,
            'properties': {
                'property': {
                    '@name': 'baseosci.overall-result',
                    '@value': overall_result
                }
            },
            'dummy_testsuites': results
        }
    }
    return xunit_dict


def make_xunit_string(tests):
    """
    Generate an xunit XML string representation for a list of KCIDBTest objects.

    Args:
        tests (list): A list of KCIDBTest objects representing various test results.

    Returns:
        str: The XML string representation of the xunit data.

    """
    if not tests:
        return None
    try:
        xunit_dict = make_xunit_dict(tests)
        xunit_xml_data = dict_to_xml(xunit_dict)
        return compress_and_b64encode(xunit_xml_data)
    except Exception:  # pylint: disable=broad-except
        LOGGER.exception("An error occurred while generating the xunit string")
        return None


def unknown_issues(tests, status, issueoccurrences):
    """Get a list of unknown issues that need to be reported."""
    return [t for t in tests if (t.status == status) and not t.waived and (
        # unknown issues
        not (test_issueoccurrences := [i for i in issueoccurrences if t.id == i.test_id]) or
        # regressions
        any(o.is_regression for o in test_issueoccurrences)
    )]


def dw_checkout(checkout_id):
    """Return a DW checkout."""
    return datawarehouse.Datawarehouse(
        os.environ['DATAWAREHOUSE_URL'],
        token=os.environ['DATAWAREHOUSE_TOKEN_GATING_REPORTER'],
        session=SESSION).kcidb.checkouts.get(id=checkout_id).all.get()


def koji_instance(checkout):
    """Return the configuration of a matching Koji instance or None."""
    if checkout.misc.get('brew_task_id'):
        for instance in SUPPORTED_KOJI_INSTANCES:
            for provenance in checkout.misc.get('provenance', []):
                if provenance['url'].startswith(instance['web_url']):
                    return instance
    return None


def report(message_type, checkout_id):
    """Report via an OSCI gating message for a checkout."""
    LOGGER.info('Gathering data for %s: %s', message_type, checkout_id)

    dw_all = dw_checkout(checkout_id)
    checkout = dw_all.checkouts[0]
    if not (instance := koji_instance(checkout)):
        LOGGER.debug('  ignoring, not a Brew build')
        return

    # groupby requires sorted data based on the key
    sorted_builds = sorted(dw_all.builds, key=lambda b: b.architecture)
    for architecture, grouped_builds in itertools.groupby(sorted_builds,
                                                          key=lambda b: b.architecture):
        grouped_builds = list(grouped_builds)
        build_ids = [b.id for b in grouped_builds]
        message = {
            'contact': {
                'name': 'CKI (Continuous Kernel Integration)',
                'team': 'CKI',
                'docs': 'https://cki-project.org',
                'url': 'https://gitlab.com/cki-project',
                'irc': 'Slack #team-kernel-cki',
                'email': 'cki-project@redhat.com',
                'environment': 'prod',
            },
            'run': {
                'url': f'{checkout.manager.api.host}/kcidb/checkouts/{checkout.misc["iid"]}',
                'log': f'{checkout.manager.api.host}/kcidb/checkouts/{checkout.misc["iid"]}',
            },
            'artifact': {
                'type': instance['artifact_type'],
                'id': checkout.misc.get('brew_task_id'),
                'issuer': checkout.attributes.get('contacts', ['CKI'])[0],
                'component': checkout.misc['source_package_name'],
                'variant': None,
                'nvr': f'{checkout.misc["source_package_name"]}-{checkout.misc["kernel_version"]}',
                'scratch': checkout.misc.get('scratch', True),
            },
            'system': [{
                'os': ','.join(sorted({b.misc.get('kpet_tree_name', '')
                                       for b in grouped_builds})),
                'provider': 'beaker',
                'architecture': architecture,
            }],
            'pipeline': {
                'id': f'{checkout.id}-{architecture}',
                'name': 'cki-gating',
            },
            'test': {
                'type': f'tier1-{architecture}',
                'category': 'functional',
                'namespace': 'cki',
                'xunit': '',
            },
            'generated_at': datetime.datetime.utcnow().isoformat() + 'Z',
            'version': '1.1.14',
        }

        # Only add test results to complete test runs. We cannot rely on empty
        # test lists because of possible delays or super quick results!
        if message_type == 'complete':
            all_tests = [t for t in dw_all.tests if t.build_id in build_ids]
            if failures := unknown_issues(all_tests, 'FAIL', dw_all.issueoccurrences):
                test_result = {'result':  'failed', 'xunit': make_xunit_string(failures)}
            # don't gate errors (https://gitlab.com/cki-project/umb-messenger/-/issues/31)
            elif unknown_issues(all_tests, 'ERROR', dw_all.issueoccurrences):
                test_result = {'result': 'info', 'note': 'test aborted, ignoring the result...'}
            else:
                test_result = {'result': 'passed'}
            message['test'].update(test_result)

        topic = f'/topic/VirtualTopic.eng.ci.cki.{instance["topic"]}.test.{message_type}'

        if misc.is_production():
            LOGGER.info('Sending message to %s', topic)
            stomp.StompClient().send_message(message, topic)
        else:
            LOGGER.info('Production mode would send %s to %s', message, topic)


def process_message(body=None, **_):
    """Filter and process messages if requested."""
    object_type = body['object_type']
    object_id = misc.get_nested_key(body, 'object/id')

    LOGGER.info('Processing message for %s %s', object_type, object_id)

    if object_type != 'checkout':
        LOGGER.debug('  ignoring, unsupported object type: %s', object_type)
        return

    if misc.get_nested_key(body, 'object/misc/retrigger', False):
        LOGGER.debug('  ignoring, retriggered checkout')
        return

    match body['status']:
        case 'build_setups_finished':
            report('running', object_id)
        case 'ready_to_report':
            report('complete', object_id)
        case status:
            LOGGER.debug('  ignoring, unsupported message status: %s', status)


def main(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Run main loop."""
    parser = argparse.ArgumentParser(description='Send UMB messages for CKI results')
    parser.add_argument('--message-type', choices=['running', 'complete'],
                        help='Message type to send')
    parser.add_argument('--checkout-id', help='Checkout ID to report')
    args = parser.parse_args(argv)

    if args.message_type and args.checkout_id:
        report(args.message_type, args.checkout_id)
        return

    misc.sentry_init(sentry_sdk)
    metrics.prometheus_init()

    messagequeue.MessageQueue().consume_messages(
        os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.exchange.webhooks'),
        os.environ['GATING_REPORTER_ROUTING_KEYS'].split(),
        process_message,
        queue_name=os.environ.get('GATING_REPORTER_QUEUE'),
    )


if __name__ == '__main__':
    main()
