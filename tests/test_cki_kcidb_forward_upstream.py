"""Test the kcidb forwarder."""
import copy
import unittest
from unittest import mock

from cki_lib.kcidb import MAJOR_VERSION
from cki_lib.kcidb import MINOR_VERSION

from tests.utils import tear_down_registry

tear_down_registry()
# pylint: disable=wrong-import-position
from cki.kcidb import forward_upstream  # noqa: E402

# pylint: enable=wrong-import-position

DW_KCIDB_SCHEMA_VERSION = {'major': MAJOR_VERSION, 'minor': MINOR_VERSION}


class TestKCIDBForwarder(unittest.TestCase):
    """Test the kcidb forwarder."""

    def test_callback_submit(self):
        """Test that messages are submitted."""
        message = forward_upstream.KCIDBMessage()
        message.submit = mock.Mock()
        message.add = mock.Mock()

        message.checkouts = ['foo']
        message.callback()
        self.assertTrue(message.submit.called)
        self.assertFalse(message.add.called)

    def test_callback_add(self):
        """Test that messages are added to the list."""
        message_ok = {
            'object_type': 'test',
            'status': 'new',
            'id': 1,
            'iid': 1,
            'object': {
                'foo': 'bar',
                'misc': {
                    'is_public': True
                }
            }
        }
        message = forward_upstream.KCIDBMessage()
        message.submit = mock.Mock()
        message.add = mock.Mock()

        ack_fn = mock.Mock()

        # Message has status != new
        message_not_new = copy.deepcopy(message_ok)
        message_not_new['status'] = 'foobar'
        message.callback(message_not_new, ack_fn)
        self.assertFalse(message.add.called)
        self.assertTrue(ack_fn.called)
        ack_fn.reset_mock()

        # Message has is_public == False
        message_private = copy.deepcopy(message_ok)
        message_private['object']['misc']['is_public'] = False
        message.callback(message_private, ack_fn)
        self.assertFalse(message.add.called)
        self.assertTrue(ack_fn.called)
        ack_fn.reset_mock()

        # Message is ok
        message.callback(message_ok, ack_fn)
        message.add.assert_called_with(
            'test', {'foo': 'bar', 'misc': {'is_public': True}}, ack_fn)
        self.assertTrue(message.add.called)
        self.assertFalse(message.submit.called)
        # This message is acked when submitted.
        self.assertFalse(ack_fn.called)

    def test_kcidbmessage_add_checkout(self):
        """Test KCIDBMessage add checkout."""
        ack_fn = mock.Mock()
        message = forward_upstream.KCIDBMessage()
        self.assertEqual(message.checkouts, [])

        message.add('checkout', {'foo': 'bar'}, ack_fn)
        self.assertEqual(message.checkouts, [{'foo': 'bar'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn])

        message.add('checkout', {'bar': 'foo'}, ack_fn)
        self.assertEqual(message.checkouts, [{'foo': 'bar'}, {'bar': 'foo'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn, ack_fn])

    def test_kcidbmessage_add_build(self):
        """Test KCIDBMessage add build."""
        ack_fn = mock.Mock()
        message = forward_upstream.KCIDBMessage()
        self.assertEqual(message.builds, [])

        message.add('build', {'foo': 'bar'}, ack_fn)
        self.assertEqual(message.builds, [{'foo': 'bar'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn])

        message.add('build', {'bar': 'foo'}, ack_fn)
        self.assertEqual(message.builds, [{'foo': 'bar'}, {'bar': 'foo'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn, ack_fn])

    def test_kcidbmessage_add_test(self):
        """Test KCIDBMessage add test."""
        ack_fn = mock.Mock()
        message = forward_upstream.KCIDBMessage()
        self.assertEqual(message.tests, [])

        message.add('test', {'foo': 'bar'}, ack_fn)
        self.assertEqual(message.tests, [{'foo': 'bar'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn])

        message.add('test', {'bar': 'foo'}, ack_fn)
        self.assertEqual(message.tests, [{'foo': 'bar'}, {'bar': 'foo'}])
        self.assertEqual(message.msg_ack_callbacks, [ack_fn, ack_fn])

    def test_kcidbmessage_add_other(self):
        """Test KCIDBMessage add unhandled."""
        message = forward_upstream.KCIDBMessage()
        self.assertEqual(message.tests, [])

        self.assertRaises(KeyError, message.add, 'foo',
                          {'foo': 'bar'}, mock.Mock())

    def test_kcidbmessage_encoded(self):
        """Test KCIDBMessage encoded property."""
        checkout = {'origin': 'redhat',
                    'id': 'redhat:92b8f402aa964f209772e30190af5de818af996c'}
        build = {'origin': 'test',
                 'checkout_id': 'redhat:92b8f402aa964f209772e30190af5de818af996c',
                 'id': 'test:1'}
        test = {'origin': 'test', 'build_id': 'test:1', 'id': 'test:1'}

        message = forward_upstream.KCIDBMessage()
        message.version = DW_KCIDB_SCHEMA_VERSION
        message.add('checkout', checkout, None)
        message.add('build', build, None)
        message.add('test', test, None)

        self.assertDictEqual(
            {
                'version': DW_KCIDB_SCHEMA_VERSION,
                'checkouts': [checkout],
                'builds': [build],
                'tests': [test],
            }, message.encoded
        )

    @mock.patch('cki.kcidb.forward_upstream.kcidb.io.schema.LATEST.validate')
    def test_kcidbmessage_encoded_validated(self, validate):
        """Test kcidb validate_exactly is called."""
        common = {'origin': 'a'}
        checkout = {**common, 'id': 'a:foo'}
        build = {**common, 'id': 'a:bar'}
        test = {**common, 'id': 'a:baz'}
        expected_msg = {
            'version': DW_KCIDB_SCHEMA_VERSION,
            'checkouts': [checkout], 'builds': [build], 'tests': [test]
        }

        message = forward_upstream.KCIDBMessage()
        message.version = DW_KCIDB_SCHEMA_VERSION
        message.checkouts.append(checkout)
        message.builds.append(build)
        message.tests.append(test)

        self.assertDictEqual(expected_msg, message.encoded)
        validate.assert_called_with(expected_msg)

    def test_kcidbmessage_len(self):
        """Test KCIDBMessage length property."""
        message = forward_upstream.KCIDBMessage()
        self.assertEqual(0, len(message))

        message.checkouts = [{}]
        message.builds = [{}, {}]
        message.tests = [{}, {}, {}]
        self.assertEqual(6, len(message))

    def test_kcidbmessage_clear(self):
        """Test KCIDBMessage clear method."""
        rev, build, test = {'foo': 'foo'}, {'bar': 'bar'}, {'foo': 'bar'}

        message = forward_upstream.KCIDBMessage()
        message.version = DW_KCIDB_SCHEMA_VERSION
        message.checkouts.append(rev)
        message.builds.append(build)
        message.tests.append(test)
        message.msg_ack_callbacks.append(mock.Mock())

        properties = (
            'checkouts',
            'builds',
            'tests',
            'msg_ack_callbacks',
        )

        for prop_name in properties:
            prop = getattr(message, prop_name)
            self.assertEqual(1, len(prop), prop_name)

        message.clear()

        for prop_name in properties:
            prop = getattr(message, prop_name)
            self.assertEqual(0, len(prop), prop_name)
        self.assertIsNone(message.version)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    def test_kcidbmessage_submit(self):
        """Test KCIDBMessage submit method."""
        message = forward_upstream.KCIDBMessage()
        message.kcidb_client = mock.Mock()
        message.version = DW_KCIDB_SCHEMA_VERSION
        message.clear = mock.Mock()
        message.ack_messages = mock.Mock()

        message.submit()
        message.kcidb_client.submit.assert_called_with(message.encoded)
        self.assertTrue(message.clear.called)
        self.assertTrue(message.ack_messages.called)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_kcidbmessage_submit_dryrun(self):
        """Test KCIDBMessage submit method."""
        kcidb_mock = mock.Mock()
        message = forward_upstream.KCIDBMessage()
        message.clear = mock.Mock()
        message.ack_messages = mock.Mock()

        message.submit()
        self.assertFalse(kcidb_mock.submit.called)
        self.assertTrue(message.clear.called)
        self.assertTrue(message.ack_messages.called)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
    @mock.patch.dict('os.environ', {'MAX_BATCH_SIZE': '0'})
    def test_kcidbmessage_submit_acks_invalid_kcidb_payload(self):
        """Test KCIDBMessage submit acks invalid kcidb bodies logging their error"""
        message_ok = {
            'object_type': 'test',
            'status': 'new',
            'id': 1,
            'iid': 1,
            'object': {
                'status': 'MISS',  # MISS was introduced by KCIDBv4.2
                'misc': {
                    'is_public': True,
                    'kcidb': {'version': {'major': 4, 'minor': 1}},
                }
            },
        }
        message = forward_upstream.KCIDBMessage()
        ack_fn = mock.Mock()

        with self.assertLogs(forward_upstream.LOGGER, "ERROR") as log_ctx:
            message.callback(message_ok, ack_fn)

        self.assertTrue(log_ctx.output[0].startswith(
            f"ERROR:{forward_upstream.LOGGER.name}:"
            "Tried to submit invalid KCIDB data upstream. The message will be acked."
        ))
        ack_fn.assert_called_once()

    def test_kcidbmessage_add_set_version(self):
        """Test KCIDBMessage add sets message version."""
        message = forward_upstream.KCIDBMessage()

        message.add(
            'checkout', {'misc': {'kcidb': {
                'version': DW_KCIDB_SCHEMA_VERSION}}}, None)
        self.assertDictEqual(
            DW_KCIDB_SCHEMA_VERSION,
            message.version,
        )

        # New message does not replace it.
        message.add(
            'checkout', {'misc': {'kcidb': {
                'version': {'major': 4, 'minor': 1}}}}, None)
        self.assertDictEqual(
            DW_KCIDB_SCHEMA_VERSION,
            message.version,
        )

        # New message replaces it only after clear.
        message.clear()
        message.add(
            'checkout', {'misc': {'kcidb': {
                'version': {'major': 4, 'minor': 1}}}}, None)
        self.assertDictEqual(
            {'major': 4, 'minor': 1},
            message.version,
        )

    def test_ack_messages(self):
        """Test ack_messages does what is expected to do."""
        message = forward_upstream.KCIDBMessage()
        message.msg_ack_callbacks = [
            mock.Mock(),
            mock.Mock()
        ]
        message.ack_messages()
        for ack_fn in message.msg_ack_callbacks:
            self.assertTrue(ack_fn.called)

    def test_add_remove_misc(self):
        """Test that the some keys are removed from misc."""
        message = forward_upstream.KCIDBMessage()
        checkouts = [
            {'foo': 'bar', 'misc': {'bar': 'bar', 'kcidb': {'foo': 'foo'}}},
            {'foo': 'bar', 'misc': {'bar': 'bar', 'is_public': True}},
            {'foo': 'bar', 'misc': {
                'bar': 'bar', 'is_public': False, 'kcidb': {'foo': 'foo'}}},
        ]

        for rev in checkouts:
            message.add('checkout', rev, None)

        self.assertListEqual(
            [
                {'foo': 'bar', 'misc': {'bar': 'bar'}},
                {'foo': 'bar', 'misc': {'bar': 'bar'}},
                {'foo': 'bar', 'misc': {'bar': 'bar'}},
            ],
            message.checkouts
        )

    def test_sanitize_tests(self):
        """Test _sanitize_test."""
        message = forward_upstream.KCIDBMessage()
        message.version = DW_KCIDB_SCHEMA_VERSION
        message.tests = [
            {'path': 'foobar'},
            {'path': 'foo bar'},
            {'path': ' foo bar'},
            {'path': 'foo   bar'},
        ]

        message._sanitize_test()  # pylint: disable=protected-access

        for test in message.tests:
            self.assertNotIn(' ', test['path'])

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_callback_submit_on_many_messages(self):
        """Test KCIDBMessage add calls submit when there are many queued messages."""
        message_ok = {
            'object_type': 'test',
            'status': 'new',
            'id': 1,
            'iid': 1,
            'object': {
                'foo': 'bar',
                'misc': {
                    'is_public': True
                }
            }
        }
        message = forward_upstream.KCIDBMessage()
        message.submit = mock.Mock()
        message.builds = [{'id': i} for i in range(message.max_batch_size)]

        ack_fn = mock.Mock()
        message.callback(message_ok, ack_fn)
        self.assertTrue(message.submit.called)

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_callback_delete_git_commit_hash(self):
        """Test git_commit_hash is deleted if no git_repository_url is present"""
        message_ok = {
            'object_type': 'checkout',
            'status': 'new',
            'id': 1,
            'iid': 1,
            'object': {
                'git_commit_hash': 'hash',
                'misc': {
                    'is_public': True
                }
            }
        }
        message = forward_upstream.KCIDBMessage()
        message.add = mock.Mock()

        ack_fn = mock.Mock()
        message.callback(message_ok, ack_fn)
        message.add.assert_called_with(
            'checkout',
            {'misc': {'is_public': True}},
            mock.ANY
        )

    @mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=False))
    def test_callback_no_delete_git_commit_hash(self):
        """Test git_commit_hash is not deleted if git_repository_url is present"""
        message_ok = {
            'object_type': 'checkout',
            'status': 'new',
            'id': 1,
            'iid': 1,
            'object': {
                'git_commit_hash': 'hash',
                'git_repository_url': 'url',
                'misc': {
                    'is_public': True
                }
            }
        }
        message = forward_upstream.KCIDBMessage()
        message.add = mock.Mock()

        ack_fn = mock.Mock()
        message.callback(message_ok, ack_fn)
        message.add.assert_called_with(
            'checkout',
            {'git_commit_hash': 'hash', 'git_repository_url': 'url', 'misc': {'is_public': True}},
            mock.ANY
        )
