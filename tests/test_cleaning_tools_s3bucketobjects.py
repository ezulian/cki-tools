"""Test CKI Cleaning Tools."""
import unittest
from unittest import mock

from cki.cleaning_tools.s3bucketobjects import S3BucketObjects
from cki.cleaning_tools.s3bucketobjects import delete_bucket_object
from cki.cleaning_tools.s3bucketobjects import get_bucket_url_size


class TestS3BucketObjects(unittest.TestCase):
    """Test S3 Bucket Objects."""
    @unittest.mock.patch('boto3.Session')
    def test_init(self, mock_session):
        """Test __init__() method in the S3BucketObjects() class."""
        custom_client = 's3control'
        s3_bucket = S3BucketObjects(client=custom_client)
        mock_session.assert_called_once_with()
        mock_session().client.assert_called_once_with(custom_client)
        self.assertEqual(s3_bucket.bucket, mock_session().client())

    def test_get_bucket_size_with_contents(self):
        """Test get_bucket_size() with contents in the S3BucketObjects() class."""
        mock_page = {'Contents': [{'Size': 10}, {'Size': 20}, {'Size': 30}]}
        mock_paginator = mock.MagicMock()
        mock_paginator.paginate.return_value = [mock_page]
        mock_client = mock.MagicMock()
        mock_client.get_paginator.return_value = mock_paginator

        bucket = S3BucketObjects()
        bucket.bucket = mock_client

        size = bucket.get_bucket_size(bucket='my-bucket', prefix='my-prefix')
        self.assertEqual(size, 60)

    def test_get_bucket_size_without_contents(self):
        """Test get_bucket_size() without contents in the S3BucketObjects() class."""
        mock_page = {'Contents': []}
        mock_paginator = mock.MagicMock()
        mock_paginator.paginate.return_value = [mock_page]
        mock_client = mock.MagicMock()
        mock_client.get_paginator.return_value = mock_paginator

        bucket = S3BucketObjects()
        bucket.bucket = mock_client

        size = bucket.get_bucket_size(bucket='my-bucket', prefix='my-prefix')
        self.assertEqual(size, 0)

    def test_get_bucket_size_with_no_prefix(self):
        """Test get_bucket_size() with no prefix in the S3BucketObjects() class."""
        mock_page = {'Contents': [{'Size': 10}, {'Size': 20}, {'Size': 30}]}
        mock_paginator = mock.MagicMock()
        mock_paginator.paginate.return_value = [mock_page]
        mock_client = mock.MagicMock()
        mock_client.get_paginator.return_value = mock_paginator

        bucket = S3BucketObjects()
        bucket.bucket = mock_client

        size = bucket.get_bucket_size(bucket='my-bucket')
        self.assertEqual(size, 60)


class TestDeleteBucketObject(unittest.TestCase):
    """Test delete_bucket_object() function."""
    @unittest.mock.patch('boto3.resource')
    def test_delete_bucket_object(self, mock_resource):
        """Check that the delete_bucket_object() correctly interacts with the boto3 resource."""
        mock_bucket = mock.MagicMock()
        mock_object = mock.MagicMock()
        mock_objects = mock.MagicMock()
        mock_objects.filter.return_value = [mock_object]
        mock_bucket.objects = mock_objects
        mock_resource.return_value.Bucket.return_value = mock_bucket

        bucket_name = "test-bucket"
        s3_config = {'buckets': [{'name': bucket_name}]}
        folder_path = "folder"

        delete_bucket_object(s3_config, folder_path)

        mock_resource.assert_called_with('s3')
        mock_resource.return_value.Bucket.assert_called_with(bucket_name)
        mock_objects.filter.assert_called_with(Prefix=folder_path)
        mock_bucket.delete_objects.assert_called_with(
            Delete={'Objects': [{'Key': mock_object.key}]})

    @unittest.mock.patch('boto3.resource')
    def test_delete_bucket_object_invalid_config(self, mock_resource):
        """Test the delete_bucket_object() with an invalid S3 configuration.

        Check that the function raises an AssertionError when the S3 configuration
        does not contain a 'buckets' key.
        """
        s3_config = {'invalid_key': [{'name': 'test-bucket'}]}
        folder_path = "folder"

        with self.assertRaises(AssertionError):
            delete_bucket_object(s3_config, folder_path)

    @unittest.mock.patch('boto3.resource')
    def test_delete_bucket_object_no_buckets(self, mock_resource):
        """Test the delete_bucket_object() with an empty 'buckets' list in the S3 configuration."""
        s3_config = {'buckets': []}
        folder_path = "folder"

        with self.assertRaises(AssertionError):
            delete_bucket_object(s3_config, folder_path)

    @unittest.mock.patch('boto3.resource')
    def test_delete_bucket_object_bucket_no_name(self, mock_resource):
        """Test the delete_bucket_object() with a bucket dictionary without a 'name' key."""
        s3_config = {'buckets': [{'invalid_key': 'test-bucket'}]}
        folder_path = "folder"

        with self.assertRaises(AssertionError):
            delete_bucket_object(s3_config, folder_path)


class TestGetBucketUrlSize(unittest.TestCase):
    """Test get_bucket_url_size() function."""
    @mock.patch('cki.cleaning_tools.s3bucketobjects.S3BucketObjects.get_bucket_size')
    def test_get_bucket_url_size(self, get_bucket_size):
        """Test get_bucket_url_size() positive flow."""
        s3_config = {'buckets': [{'name': 'my-bucket'}]}
        pipeline_ids = [100, 101]
        expected_result = {'buckets': [
            {'url': 'https://s3.amazonaws.com/my-bucket/index.html?prefix=trusted-artifacts/100/',
             'size': '100.0 B', 'bucket_name': 'my-bucket'},
            {'url': 'https://s3.amazonaws.com/my-bucket/index.html?prefix=trusted-artifacts/101/',
             'size': '100.0 B', 'bucket_name': 'my-bucket'}], 'total_size': 200}
        get_bucket_size.return_value = 100
        result = get_bucket_url_size(pipeline_ids, s3_config)
        self.assertEqual(result, expected_result)

    @mock.patch('cki.cleaning_tools.s3bucketobjects.S3BucketObjects.get_bucket_size')
    def test_get_bucket_url_size_empty_buckets(self, get_bucket_size):
        """Test get_bucket_url_size() empty buckets flow."""
        s3_config = {'buckets': [{'name': 'my-bucket'}]}
        pipeline_ids = []
        expected_result = {'buckets': [], 'total_size': 0}
        get_bucket_size.return_value = 0
        result = get_bucket_url_size(pipeline_ids, s3_config)
        self.assertEqual(result, expected_result)
