"""Datawarehouse UMB Submitter tests."""
from http import HTTPStatus
import json
import os
import unittest
from unittest import mock

import pika
import requests
from requests.exceptions import HTTPError
import responses

from cki.kcidb.datawarehouse_umb_submitter import LOGGER
from cki.kcidb.datawarehouse_umb_submitter import Receiver
from cki.kcidb.datawarehouse_umb_submitter import main

DW_API = 'http://server'


class TestDatawarehouseUMBSubmitter(unittest.TestCase):
    """Test cki.kcidb.datawarehouse_umb_submitter."""

    body = {
        "version": {
            "major": 4,
            "minor": 0
        },
        "checkouts": [],
        "builds": [],
        "tests": [],
    }

    callback_kwargs = {
        'body': body,
        'routing_key': 'routing_key',
        'headers': None,
        'ack_fn': None,
        'timeout': False,
    }

    datawarehouse_environ = {
        'DATAWAREHOUSE_URL': DW_API,
        'DATAWAREHOUSE_TOKEN_SUBMITTER': 'datawarehouse_token',
    }

    @mock.patch.object(Receiver, 'callback')
    @mock.patch('pika.BlockingConnection')
    @mock.patch.dict(
        os.environ,
        {
            'RABBITMQ_HOST': 'host',
            'RABBITMQ_PORT': '123',
            'RABBITMQ_USER': 'user',
            'RABBITMQ_PASSWORD': 'password',
            'RABBITMQ_QUEUE': 'queue',
            'RABBITMQ_ROUTING_KEYS': 'routing1 routing2',
            **datawarehouse_environ,
        }
    )
    def test_main(self, connection, callback):
        connection().channel().consume.return_value = [(
            mock.Mock(routing_key='routing_key', delivery_tag='delivery_tag'),
            pika.BasicProperties(),
            json.dumps(self.body),
        )]

        main()
        callback.assert_called_with(**self.callback_kwargs)

    @responses.activate
    @mock.patch.dict(os.environ, datawarehouse_environ)
    def test_callback_valid(self):
        """Test callback when passed valid KCIDB data."""
        receiver = Receiver()

        with self.subTest("Validate successful calls to submit work as expected"):
            create_request = responses.post(DW_API + '/api/1/kcidb/submit')
            receiver.callback(**self.callback_kwargs)

            responses.assert_call_count(create_request.url, 1)
            request: requests.PreparedRequest = responses.calls[0].request
            self.assertEqual(request.body, json.dumps({"data": self.body}),
                             f"Expected {create_request.url!r} to be called with {self.body!r}")

        with self.subTest("Validate BAD REQUEST calls to submit work as expected"):
            responses.calls.reset()
            create_request = responses.post(DW_API + '/api/1/kcidb/submit',
                                            status=HTTPStatus.BAD_REQUEST)

            with self.assertLogs(LOGGER, level="ERROR") as log_ctx:
                receiver.callback(**self.callback_kwargs)

            expected_log = f'ERROR:{LOGGER.name}:Failed to submit data from UMB to DataWarehouse'
            self.assertTrue(log_ctx.output[0].startswith(expected_log),
                            f"Expected {log_ctx.output[0]=!r} to start with {expected_log!r}")

            responses.assert_call_count(create_request.url, 1)
            request: requests.PreparedRequest = responses.calls[0].request
            self.assertEqual(request.body, json.dumps({"data": self.body}),
                             f"Expected {create_request.url!r} to be called with {self.body!r}")

        with self.subTest("Validate any other HTTP error raises"):
            responses.calls.reset()
            create_request = responses.post(DW_API + '/api/1/kcidb/submit',
                                            status=HTTPStatus.BAD_GATEWAY)

            with self.assertRaises(HTTPError):
                receiver.callback(**self.callback_kwargs)

            responses.assert_call_count(create_request.url, 1)
            request: requests.PreparedRequest = responses.calls[0].request
            self.assertEqual(request.body, json.dumps({"data": self.body}),
                             f"Expected {create_request.url!r} to be called with {self.body!r}")

    @mock.patch.dict(os.environ, datawarehouse_environ)
    def test_callback_invalid(self):
        """Test callback when passed invalid KCIDB data."""
        receiver = Receiver()
        with mock.patch.object(receiver.dw_api.kcidb.submit, 'create') as mock_create:
            receiver.callback(body={})
            self.assertFalse(mock_create.called)
