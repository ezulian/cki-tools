"""Test edit.py"""
import contextlib
import io
import json
from pathlib import Path
import tempfile
import unittest
from unittest import mock

from cki_lib.kcidb import KCIDBFile
from cki_lib.kcidb import MAJOR_VERSION
from cki_lib.kcidb import MINOR_VERSION

from cki.kcidb.get_test_summary import main

KCIDB_SCHEMA_VERSION = {'major': MAJOR_VERSION, 'minor': MINOR_VERSION}


class TestKCIDBGetTestSummary(unittest.TestCase):
    """Test get_test_summary.py."""

    # Copied from cki_lib
    @classmethod
    @contextlib.contextmanager
    def dump_kcidb_json(cls, kcidb_data):
        tmpdir = tempfile.TemporaryDirectory()
        path = Path(tmpdir.name, 'kcidb_data.json')
        path.write_text(json.dumps(kcidb_data))
        try:
            yield path
        finally:
            tmpdir.cleanup()

    @classmethod
    @contextlib.contextmanager
    def create_kcidb_file(cls, kcidb_data):
        with cls.dump_kcidb_json(kcidb_data) as path:
            kcidb_file = KCIDBFile(str(path))
            try:
                yield (path, kcidb_file)
            finally:
                pass

    def test_no_tests(self):
        """Verify summary is PASS if there is no test data."""
        kcidb_data = {'version': KCIDB_SCHEMA_VERSION}  # no "tests" here

        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                main([str(kcidb_file_path)])
                self.assertEqual(mock_stdout.getvalue(), 'PASS\n')

    def test_no_runs(self):
        """Verify summary is MISS if no tests were executed, i.e. have no "status"."""
        kcidb_data = {
            'version': KCIDB_SCHEMA_VERSION,
            'tests': [  # every test without "status"
                {'id': 'redhat:test_1', 'build_id': 'redhat:build1', 'origin': 'redhat'},
                {'id': 'redhat:test_2', 'build_id': 'redhat:build1', 'origin': 'redhat'}
            ],
        }

        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                main([str(kcidb_file_path)])
                self.assertEqual(mock_stdout.getvalue(), 'MISS\n')

    def test_failed_run(self):
        """Verify summary is FAIL if some tests reported fail."""
        kcidb_data = {
            'version': KCIDB_SCHEMA_VERSION,
            'tests': [
                {'id': 'redhat:test_1', 'build_id': 'redhat:build1', 'origin': 'redhat'},
                {'id': 'redhat:test_2', 'status': 'FAIL', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_3', 'status': 'PASS', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_4', 'status': 'ERROR', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_5', 'status': 'SKIP', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_6', 'status': 'MISS', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
            ],
        }

        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                main([str(kcidb_file_path)])
                self.assertEqual(mock_stdout.getvalue(), 'FAIL\n')

    def test_error_run(self):
        """Verify summary is ERROR if no tests failed and some reported errors."""
        kcidb_data = {
            'version': KCIDB_SCHEMA_VERSION,
            'tests': [
                {'id': 'redhat:test_1', 'status': 'PASS', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_2', 'status': 'ERROR', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_3', 'status': 'SKIP', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_4', 'status': 'MISS', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_5', 'build_id': 'redhat:build1', 'origin': 'redhat'},
            ],
        }

        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                main([str(kcidb_file_path)])
                self.assertEqual(mock_stdout.getvalue(), 'ERROR\n')

    def test_miss_run(self):
        """Verify summary is MISS if no tests failed or errored and some reported misses."""
        kcidb_data = {
            'version': KCIDB_SCHEMA_VERSION,
            'tests': [
                {'id': 'redhat:test_1', 'status': 'PASS', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_2', 'status': 'MISS', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_3', 'status': 'SKIP', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_4', 'build_id': 'redhat:build1', 'origin': 'redhat'},
            ],
        }

        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                main([str(kcidb_file_path)])
                self.assertEqual(mock_stdout.getvalue(), 'MISS\n')

    def test_pass_run(self):
        """Verify summary is PASS if no tests failed, errored or was missed."""
        kcidb_data = {
            'version': KCIDB_SCHEMA_VERSION,
            'tests': [
                {'id': 'redhat:test_1', 'status': 'PASS', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_2', 'status': 'PASS', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_3', 'status': 'SKIP', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_4', 'status': 'DONE', 'build_id': 'redhat:build1',
                 'origin': 'redhat'}
            ],
        }

        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                main([str(kcidb_file_path)])
                self.assertEqual(mock_stdout.getvalue(), 'PASS\n')

    def test_waived(self):
        """Verify summary is PASS if only waived tests reported issues."""
        kcidb_data = {
            'version': KCIDB_SCHEMA_VERSION,
            'tests': [
                {'id': 'redhat:test_1', 'build_id': 'redhat:build1',
                 'origin': 'redhat', 'waived': True},
                {'id': 'redhat:test_2', 'status': 'FAIL', 'build_id': 'redhat:build1',
                 'origin': 'redhat', 'waived': True},
                {'id': 'redhat:test_3', 'status': 'PASS', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_4', 'status': 'ERROR', 'build_id': 'redhat:build1',
                 'origin': 'redhat', 'waived': True},
                {'id': 'redhat:test_5', 'status': 'SKIP', 'build_id': 'redhat:build1',
                 'origin': 'redhat'},
                {'id': 'redhat:test_6', 'status': 'MISS', 'build_id': 'redhat:build1',
                 'origin': 'redhat', 'waived': True}
            ],
        }

        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                main([str(kcidb_file_path)])
                self.assertEqual(mock_stdout.getvalue(), 'PASS\n')
