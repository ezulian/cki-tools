"""Test CKI Cleaning Tools."""

import unittest
from unittest.mock import patch

import gitlab
import responses

from cki.cleaning_tools.get_gitlab_data import GetMRsInfo


class TestGetMRsInfo(unittest.TestCase):
    """Test Get Gitlab Data."""
    def setUp(self):
        """Set Up method for TestGetMRsInfo."""
        gl_instance = gitlab.Gitlab('https://instance')
        self.mock_project = gl_instance.projects.get('cki-project/cki-tools', lazy=True)
        self.git = GetMRsInfo(self.mock_project)
        self.mr_url = "https://instance/api/v4/projects/cki-project%2Fcki-tools"
        self.mock_mr_id = 100001
        self.mock_pipeline_id = 100001
        self.mock_data = [
            {'updated_at': '2001-01-01T00:00:00.111Z',
             'iid': 333000333,
             'id': 3330333,
             'state': 'success',
             'downstream_pipeline': {'id': 1110111},
             'project_id': 111000111, 'mr_iid': '100', 'attributes': {'id': 1110000111},
             'status': 'success'},
            {'updated_at': '2002-02-02T00:00:00.222Z',
             'iid': 222000222,
             'id': 2220222,
             'state': 'merged',
             'downstream_pipeline': {'id': 22200222},
             'project_id': 222000222, 'mr_iid': '100', 'attributes': {'id': 222000022},
             'status': 'failed'},
            {'updated_at': '2003-03-03T00:00:00.333Z',
             'iid': 111000111,
             'id': 1110111,
             'state': 'closed',
             'downstream_pipeline': {'id': 3330022},
             'project_id': 333000333, 'mr_iid': '100', 'attributes': {'id': 3330000333},
             'status': 'pending'}]

    def test_init(self):
        """Test __init__() method in the GetMRsInfo() class."""
        self.assertEqual(self.git.project, self.mock_project)

    @responses.activate
    def test_get_number_of_merge_requests(self):
        """Test number_of_merge_requests() method."""
        responses.add(
            responses.GET,
            f"{self.mr_url}/merge_requests",
            json=self.mock_data,
            headers={'X-Total': '555055'}
        )
        number_of_merge_requests = self.git.get_number_of_merge_requests()
        self.assertEqual(number_of_merge_requests, 555055)

    @responses.activate
    def test_merge_request_state(self):
        """Test merge_request_state() method."""
        responses.add(
            responses.GET,
            f"{self.mr_url}/merge_requests/{str(self.mock_mr_id)}",
            json={'state': 'opened'}
        )
        merge_request_state = self.git.get_merge_request_state(self.mock_mr_id)
        self.assertEqual(merge_request_state, "opened")

    @patch('cki.cleaning_tools.get_gitlab_data.GetMRsInfo.get_merge_request_state',
           return_value='opened')
    def test_check_merge_request_state_positive(self, get_merge_request_state):
        """Test check_merge_request_state() method positive."""
        merge_request_state = self.git.check_merge_request_state(self.mock_mr_id, 'opened')
        self.assertTrue(merge_request_state)

    @patch('cki.cleaning_tools.get_gitlab_data.GetMRsInfo.get_merge_request_state',
           return_value='opened')
    def test_check_merge_request_state_negative(self, get_merge_request_state):
        """Test check_merge_request_state() method negative."""
        merge_request_state = self.git.check_merge_request_state(self.mock_mr_id, 'closed')
        self.assertFalse(merge_request_state)

    @responses.activate
    def test_get_merge_request_pipelines_list(self):
        """Test get_merge_request_pipelines_list() method."""
        responses.add(
            responses.GET,
            f"{self.mr_url}/merge_requests/{str(self.mock_mr_id)}/pipelines",
            json=self.mock_data
        )
        merge_request_pipelines_list = self.git._get_merge_request_pipelines_list(self.mock_mr_id)
        self.assertEqual([merge_request_pipelines_list[0].id,
                          merge_request_pipelines_list[1].id,
                          merge_request_pipelines_list[2].id],
                         [3330333, 2220222, 1110111])

    @responses.activate
    def test_get_list_bridges_of_pipeline(self):
        """Test get_list_bridges_of_pipeline() method."""
        responses.add(
            responses.GET,
            f"{self.mr_url}/pipelines/{str(self.mock_pipeline_id)}/bridges",
            json=self.mock_data
        )
        list_jobs_of_pipeline = self.git.get_list_bridges_of_pipeline(self.mock_pipeline_id)
        self.assertEqual(list_jobs_of_pipeline, [1110111, 22200222, 3330022])

    @responses.activate
    def test_get_pipelines_attributes(self):
        """Test get_pipelines_attributes() method."""
        responses.add(
            responses.GET,
            f"{self.mr_url}/merge_requests/{str(self.mock_mr_id)}/pipelines",
            json=self.mock_data
        )
        p_attributes = self.git._get_pipelines_attributes(self.mock_mr_id)
        self.assertEqual(p_attributes, self.mock_data)

    @responses.activate
    def test_get_merge_request_pipelines_before_the_last_succeed(self):
        """Test get_merge_request_pipelines_before_the_last_succeed() method."""
        responses.add(
            responses.GET,
            f"{self.mr_url}/merge_requests/{str(self.mock_mr_id)}/pipelines",
            json=self.mock_data
        )
        mr_pipelines = self.git.get_merge_request_pipelines_before_the_last_succeed(self.mock_mr_id)

        self.assertEqual([mr_pipelines[0]['status'],
                          mr_pipelines[1]['status']],
                         ['failed', 'pending'])

    @responses.activate
    def test_get_merge_requests_by_period(self):
        """Test get_merge_requests_by_period() method."""
        created_after = '2022-10-29T08:00:00Z'
        created_before = '2022-11-28T08:00:00Z'

        responses.add(
            responses.GET,
            f"{self.mr_url}/merge_requests?state=merged"
            f"&order_by=updated_at"
            f"&created_after=2022-10-29T08%3A00%3A00Z"
            f"&created_before=2022-11-28T08%3A00%3A00Z",
            json=self.mock_data
        )
        merge_requests = self.git.get_merge_requests_by_period(created_after=created_after,
                                                               created_before=created_before)
        self.assertEqual(merge_requests,
                         {'first_mr_id': 111000111, 'last_mr_id': 333000333, 'total_mrs': 3})
