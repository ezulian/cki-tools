"""KCIDB utils tests."""
import unittest

import responses

from cki.kcidb import utils


class TestUtils(unittest.TestCase):
    """Test Job methods."""

    @responses.activate
    def test_patchset_hash(self):
        """Test patchset_hash return values."""
        responses.add(responses.GET, 'https://s/p_1', body=b'p_1')
        responses.add(responses.GET, 'https://s/p_2/', body=b'p_2')

        patch_list = [
            'https://s/p_1', 'https://s/p_2/'
        ]

        self.assertEqual('', utils.patch_list_hash([]))

        self.assertEqual(
            'a560b2642c43bb3494b79d364d9371dd4468307443d1fde7b12f51e7ba4c1b33',
            utils.patch_list_hash([patch_list[0]])
        )
        self.assertEqual(
            '8d2e6de35099fc8e1c9f98f6c107c004f414846d30f6315a23ccbe0d9a664328',
            utils.patch_list_hash(patch_list)
        )
