"""Tests for cki_tools.koji_trigger."""
from importlib import resources
import os
import unittest
from unittest import mock

from cki_lib import misc
from cki_lib import yaml
from freezegun import freeze_time

from cki_tools import koji_trigger

from . import assets


class TestNVRExtraction(unittest.TestCase):
    """Tests for KojiTrigger.get_nvr()."""

    def test_request_match(self):
        """
        Verify a corrent NVR is returned if the request string match is
        detected.
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        self.assertEqual(nvr, koji_trigger.KojiTrigger.get_nvr(
            None, {'id': 123, 'request': [request_string]}))

    def test_request_no_match(self):
        """Verify an empty string is returned if there is no match."""
        session = mock.Mock()
        session.listTasks.return_value = []
        self.assertEqual('', koji_trigger.KojiTrigger.get_nvr(
            session, {'id': 123, 'request': ['']}))

    def test_rpm_match(self):
        """
        Verify a corrent NVR is returned if we got an official build from
        dist-git.
        """
        nvr = 'kernel-3.10.0-1006.el7'
        request_string = 'git://pkgs.devel.org/rpms/kernel#123abc'

        session = mock.Mock()
        session.listBuilds.return_value = [{'nvr': nvr}]
        self.assertEqual(nvr, koji_trigger.KojiTrigger.get_nvr(
            session, {'id': 123, 'request': [request_string]}))

    def test_rpm_match_scratch(self):
        """
        Verify a corrent NVR is returned if we got a scratch build from
        dist-git.
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = 'git://pkgs.devel.org/rpms/kernel#123abc'

        session = mock.Mock()
        session.listBuilds.return_value = []
        session.listTasks.return_value = [{'request': [f'tasks/123/{nvr}']}]
        self.assertEqual(nvr, koji_trigger.KojiTrigger.get_nvr(
            session, {'id': 123, 'request': [request_string]}))

    def test_scratch_from_git(self):
        """
        Verify a corrent NVR is returned if we got a scratch build from git.
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = 'git://git.org/users/myuser/linux.git?rh/koji#abcd'

        session = mock.Mock()
        session.listBuilds.return_value = []
        session.listTasks.return_value = [{'request': [f'tasks/123/{nvr}']}]
        self.assertEqual(nvr, koji_trigger.KojiTrigger.get_nvr(
            session, {'id': 123, 'request': [request_string]}))

    def test_rpm_no_match(self):
        """Verify an empty string is returned if we can't extract the NVR."""
        request_string = 'git://pkgs.devel.org/rpms/kernel#123abc'

        session = mock.Mock()
        session.listBuilds.return_value = []
        session.listTasks.return_value = []
        self.assertEqual('', koji_trigger.KojiTrigger.get_nvr(
            session, {'id': 123, 'request': [request_string]}))


class TestGetKojiTriggers(unittest.TestCase):
    """Tests for KojiTrigger.get_triggers."""

    @staticmethod
    @mock.patch('cki_lib.gitlab.get_instance', mock.Mock())
    def _koji(pipelines=None):
        with freeze_time("2019-01-01"):
            config = {
                '.default': {
                    'send_report_to_upstream': 'False',
                    'send_report_on_success': 'False',
                },
            }
            config.update(pipelines or {})
            return koji_trigger.KojiTrigger(
                config, '', 'server_url', 'web_url', 'top_url',
            ).get_triggers(123)

    @mock.patch('koji.ClientSession')
    def test_no_request(self, mock_session):
        """
        Check we don't continue if the message doesn't contain a request
        string.
        """
        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {}

        with self.assertLogs(koji_trigger.LOGGER, 'INFO') as log:
            variables = self._koji()
            self.assertEqual(variables, [])
            self.assertEqual(len(log.output), 1)
            self.assertIn('123: Task doesn\'t have a build request, ignoring',
                          log.output[0])

    @mock.patch('koji.ClientSession')
    def test_scratch_disabled(self, mock_session):
        """
        Check we don't trigger pipeline for scratch build if the scratch build
        testing is disbled (not set).
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        cases = ({'scratch': True}, {'skip_tag': True})

        for task_options in cases:
            mock_session_instance = mock_session.return_value
            mock_session_instance.getTaskInfo.return_value = {
                'id': 123,
                'request': [request_string, 'tag', task_options],
            }

            with self.assertLogs(koji_trigger.LOGGER, 'INFO') as log:
                variables = self._koji({'pipeline': {
                    'rpm_release': 'el7',
                    'source_package_name': 'kernel',
                    'package_name': 'kernel'
                }})
                self.assertEqual(variables, [])
                self.assertIn('Scratch build testing disabled in trigger', log.output[-2])
                self.assertIn(f'123: Pipeline for {nvr} not configured!', log.output[-1])

    @mock.patch('koji.ClientSession')
    def test_no_nvr(self, mock_session):
        """Check we don't continue if no NVR is found."""
        request_string = 'cli-build/1550507696.83405.UHlEWvSP/nothing_here'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'request': [request_string, 'tag', {}],
        }

        with self.assertLogs(koji_trigger.LOGGER, 'DEBUG') as log:
            variables = self._koji()
            self.assertEqual(variables, [])
            self.assertIn('Can\'t get NVR from task', log.output[-1])

    @mock.patch('koji.ClientSession')
    def test_no_trigger_match(self, mock_session):
        """Check we don't continue if there is no trigger for given build."""
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'request': [request_string, 'tag', {}],
        }

        with self.assertLogs(koji_trigger.LOGGER, 'DEBUG') as log:
            variables = self._koji()
            self.assertEqual(variables, [])
            self.assertIn(f'Pipeline for {nvr} not configured', log.output[-1])

    @mock.patch('koji.ClientSession')
    def test_bad_source_package_name(self, mock_session):
        """Check we don't continue if the package name doesn't match."""
        nvr = 'python-metakernel-0.21.0-1.fc30'
        request_string = 'git://example.org/rpms/python-metakernel#1a2b'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'request': [request_string, 'tag', {}],
        }
        mock_session_instance.listBuilds.return_value = [{'nvr': nvr}]

        with self.assertLogs(koji_trigger.LOGGER, 'DEBUG') as log:
            variables = self._koji()
            self.assertEqual(variables, [])
            self.assertIn(f'Pipeline for {nvr} not configured', log.output[-1])

    @mock.patch('koji.ClientSession')
    def test_scratch_enabled(self, mock_session):
        """
        Check we trigger a pipeline for scratch build if the scratch build
        testing is enabled.
        """
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        cases = ({'scratch': True}, {'skip_tag': True})

        for task_options in cases:
            mock_session_instance = mock_session.return_value
            mock_session_instance.getTaskInfo.return_value = {
                'id': 123,
                'owner': 1,
                'request': [request_string, 'tag', task_options]
            }
            mock_session_instance.listBuilds.return_value = [{'nvr': 'kernel-nvr'}]

            variables = self._koji({'pipeline': {
                'rpm_release': 'el7',
                'source_package_name': 'kernel',
                'package_name': 'kernel',
                '.test_scratch': 'True',
            }})
            self.assertNotEqual(variables, [])
            self.assertTrue(misc.strtobool(variables[0].get('scratch')))

    @freeze_time("2019-01-01")
    @mock.patch('koji.ClientSession')
    def test_pipeline_triggered(self, mock_session):
        """Check we trigger a pipeline."""
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'owner': 1,
            'request': [
                request_string,
                'tag',
                {'arch_override': 'x86_86 ppc64le'},
            ],
        }
        mock_session_instance.listBuilds.return_value = [{'nvr': 'kernel-nvr'}]
        mock_session_instance.getUser.return_value = {'name': 'name'}

        variables = self._koji({'pipeline': {
            'rpm_release': 'el7',
            'source_package_name': 'kernel',
            'package_name': 'kernel',
            '.coprs': ['user/repo1', 'user/repo2'],
        }})
        expected_variables = [
            {'name': 'pipeline',
             'rpm_release': 'el7',
             'source_package_name': 'kernel',
             'package_name': 'kernel',
             'kernel_version': '3.10.0-1006.el7.test',
             'brew_task_id': '123',
             'title': 'Koji/Brew: Task 123 - kernel',
             'scratch': 'false',
             'submitter': 'name@redhat.com',
             'checkout_contacts': '["name@redhat.com"]',
             'architectures': 'x86_86 ppc64le',
             'server_url': 'server_url',
             'web_url': 'web_url',
             'top_url': 'top_url',
             'send_report_on_success': 'False',
             'send_report_to_upstream': 'False',
             }
        ]
        self.assertEqual(variables, expected_variables)

    @freeze_time("2019-01-01")
    @mock.patch('koji.ClientSession')
    def test_explicit_name(self, mock_session):
        """Check we don't override the name variable."""
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'owner': 1,
            'request': [
                request_string,
                'tag',
                {'arch_override': 'x86_86 ppc64le'},
            ],
        }
        mock_session_instance.listBuilds.return_value = [{'nvr': 'kernel-nvr'}]
        mock_session_instance.getUser.return_value = {'name': 'name'}

        variables = self._koji({'pipeline': {
            'rpm_release': 'el7',
            'source_package_name': 'kernel',
            'package_name': 'kernel',
            'name': 'foo',
            '.coprs': ['user/repo1', 'user/repo2'],
        }})
        expected_variables = [
            {'name': 'foo',
             'rpm_release': 'el7',
             'source_package_name': 'kernel',
             'package_name': 'kernel',
             'kernel_version': '3.10.0-1006.el7.test',
             'brew_task_id': '123',
             'title': 'Koji/Brew: Task 123 - kernel',
             'scratch': 'false',
             'submitter': 'name@redhat.com',
             'checkout_contacts': '["name@redhat.com"]',
             'architectures': 'x86_86 ppc64le',
             'server_url': 'server_url',
             'web_url': 'web_url',
             'top_url': 'top_url',
             'send_report_on_success': 'False',
             'send_report_to_upstream': 'False',
             }
        ]
        self.assertEqual(variables, expected_variables)

    @freeze_time("2019-01-01")
    @mock.patch('koji.ClientSession')
    def test_nvr_with_letters(self, mock_session):
        """
        Check trigger matching works if a kernel NVR contains letter between
        the package name and release. Regression test, as a residue of previous
        match implementation this didn't work but it should.
        """
        nvr = 'kernel-3.10.0.rc8.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'owner': 1,
            'request': [request_string, 'tag', {}],
        }
        mock_session_instance.listBuilds.return_value = [{'nvr': 'kernel-nvr'}]
        mock_session_instance.getUser.return_value = {'name': 'name'}

        variables = self._koji({'pipeline': {
            'rpm_release': 'el7',
            'source_package_name': 'kernel',
            'package_name': 'kernel'
        }})
        expected_variables = [
            {'name': 'pipeline',
             'rpm_release': 'el7',
             'source_package_name': 'kernel',
             'package_name': 'kernel',
             'kernel_version': '3.10.0.rc8.el7.test',
             'brew_task_id': '123',
             'title': 'Koji/Brew: Task 123 - kernel',
             'scratch': 'false',
             'submitter': 'name@redhat.com',
             'checkout_contacts': '["name@redhat.com"]',
             'server_url': 'server_url',
             'web_url': 'web_url',
             'top_url': 'top_url',
             'send_report_on_success': 'False',
             'send_report_to_upstream': 'False',
             }
        ]
        self.assertEqual(variables, expected_variables)

    @freeze_time("2019-01-01")
    @mock.patch('koji.ClientSession')
    def test_architectures(self, mock_session):
        """Check architectures is set correctly if the user overrides it."""
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'owner': 1,
            'request': [
                request_string,
                'tag',
                {'arch_override': 'x86_86 ppc64le'}
            ],
        }
        mock_session_instance.listBuilds.return_value = [{'nvr': 'kernel-nvr'}]
        mock_session_instance.getUser.return_value = {'name': 'name'}

        variables = self._koji({'pipeline': {
            'rpm_release': 'el7',
            'source_package_name': 'kernel',
            'package_name': 'kernel',
            'architectures': 'ppc64le',
        }})
        expected_variables = [
            {'name': 'pipeline',
             'rpm_release': 'el7',
             'source_package_name': 'kernel',
             'package_name': 'kernel',
             'kernel_version': '3.10.0-1006.el7.test',
             'brew_task_id': '123',
             'title': 'Koji/Brew: Task 123 - kernel',
             'scratch': 'false',
             'submitter': 'name@redhat.com',
             'checkout_contacts': '["name@redhat.com"]',
             'architectures': 'ppc64le',
             'server_url': 'server_url',
             'web_url': 'web_url',
             'top_url': 'top_url',
             'send_report_on_success': 'False',
             'send_report_to_upstream': 'False',
             }
        ]
        self.assertEqual(variables, expected_variables)

    @mock.patch('koji.ClientSession')
    def test_report_rules(self, mock_session):
        """Check report_rules are correctly parsed and added to the variables."""
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'owner': 1,
            'request': [request_string, 'tag', {}],
        }

        variables = self._koji({'pipeline': {
            'rpm_release': 'el7',
            'source_package_name': 'kernel',
            'package_name': 'kernel',
            '.report_rules': [
                {'if': 'something',
                 'send_cc': 'someone',
                 'send_bcc': 'someone_else'}
            ],
        }})

        self.assertEqual(
            variables[0]['report_rules'],
            '[{"if": "something", "send_cc": "someone", "send_bcc": "someone_else"}]',
        )

    @mock.patch('koji.ClientSession')
    def test_upstream_owner(self, mock_session):
        """Verify upstream owner is preferred if present."""
        nvr = 'kernel-3.10.0-1006.el7.test.src.rpm'
        request_string = f'cli-build/1550507696.83405.UHlEWvSP/{nvr}'

        mock_session_instance = mock_session.return_value
        mock_session_instance.getTaskInfo.return_value = {
            'id': 123,
            'owner': 1,
            'request': [request_string, 'tag', {
                'custom_user_metadata': {'osci': {'upstream_owner_name': 'realowner'}}
            }],
        }

        variables = self._koji({'pipeline': {
            'rpm_release': 'el7',
            'source_package_name': 'kernel',
            'package_name': 'kernel',
        }})
        self.assertEqual(variables[0]['submitter'], 'realowner@redhat.com')


class TestMain(unittest.TestCase):
    """Tests for main()."""

    @mock.patch('cki_lib.messagequeue.MessageQueue')
    @mock.patch.dict(os.environ, {
        'KOJI_TRIGGER_QUEUE': 'queue',
        'KOJI_TRIGGER_ROUTING_KEYS': 'key1 key2',
    })
    def test_messagequeue(self, mock_mq):
        """Verify listening to the message queue."""

        koji_trigger.main([])
        mock_mq.return_value.consume_messages.assert_called_with(
            'cki.exchange.webhooks', ['key1', 'key2'], mock.ANY, queue_name='queue')

    @mock.patch('cki_tools.koji_trigger.KojiTrigger.manual')
    def test_manual(self, manual):
        """Verify triggering a manual pipeline."""

        koji_trigger.main(['--task-id', '1'])
        manual.assert_called_with('1', {})


class TestParseMessage(unittest.TestCase):
    """Tests for KojiTrigger.parse_message()."""

    def test_process_message(self):
        """Verify process_message works as expected."""
        cases = (
            ('umb working',
             {'attribute': 'state', 'new': 'CLOSED',
              'info': {'request': 'foo', 'method': 'build', 'id': 1}}, 1),
            ('umb attribute',
             {'attribute': 'foo', 'new': 'CLOSED',
              'info': {'request': 'foo', 'method': 'build', 'id': 1}}, None),
            ('umb new',
             {'attribute': 'state', 'new': 'FOO',
              'info': {'request': 'foo', 'method': 'build', 'id': 1}}, None),
            ('umb request',
             {'attribute': 'state', 'new': 'CLOSED',
              'info': {'method': 'build', 'id': 1}}, None),
            ('umb method',
             {'attribute': 'state', 'new': 'CLOSED',
              'info': {'request': 'foo', 'method': 'baz', 'id': 1}}, None),
            ('umb id',
             {'attribute': 'state', 'new': 'CLOSED',
              'info': {'request': 'foo', 'method': 'build'}}, None),
            ('koji working',
             {'attribute': 'state', 'new': 1,
              'task': {'request': 'foo', 'method': 'build', 'id': 1}}, 1),
            ('koji new',
             {'attribute': 'state', 'new': 0,
              'task': {'request': 'foo', 'method': 'build', 'id': 1}}, None),
        )
        for description, body, expected in cases:
            with self.subTest(description):
                self.assertEqual(koji_trigger.KojiTrigger.parse_message(body), expected)


class TestKojiTrigger(unittest.TestCase):
    """Test KojiTrigger methods."""

    def test_message(self):
        cases = (
            ('brew-closed-scratch.yml', 54667423),
            ('brew-closed.yml', 54662772),
            ('koji-closed-scratch.yml', 104944757),
            ('koji-closed.yml', 104837525),
        )
        for file_path, expected in cases:
            with self.subTest(file_path):
                message_data = yaml.load(
                    contents=(resources.files(assets) / file_path).read_text('utf8'))
                with mock.patch('cki_tools.koji_trigger.KojiTrigger.get_triggers') as get_triggers:
                    koji_trigger.KojiTrigger(None, '', '', '', '').callback(
                        body=message_data['body'], headers=message_data['headers'])
                get_triggers.assert_called_with(expected)
