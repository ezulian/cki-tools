"""Tests for the cki.deployment_tools.secrets module."""
import contextlib
import io
import os
import pathlib
import subprocess
import tempfile
import typing
import unittest
from unittest import mock

import responses
import yaml

from cki.deployment_tools import secrets


@unittest.skipUnless(b'pbkdf' in subprocess.run(
    ['openssl', 'enc', '-help'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=False
).stdout, 'OpenSSL version too old')
class TestSecrets(unittest.TestCase):
    """Test yaml utils."""

    bar = 'U2FsdGVkX18hbgdS60BhEkxx1selyY0VvHsWawgKtlQ='

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(
        variables: typing.Dict[str, typing.Any],
    ) -> typing.Iterator[str]:
        secrets._read_secrets_file.cache_clear()  # pylint: disable=protected-access
        with tempfile.TemporaryDirectory() as directory:
            for name, values in variables.items():
                data = yaml.safe_dump(values) if isinstance(values, dict) else values
                pathlib.Path(directory, name).write_text(data, encoding='utf8')
            yield directory

    def test_variables(self) -> None:
        """Check that basic variable processing works."""
        with self._setup_secrets({
                'internal.yml': {'bar': 0, 'foo': 'qux', 'sec': 'none'},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
        }):
            self.assertEqual(secrets.variable('bar'), 0)
            self.assertEqual(secrets.variable('foo'), 'qux')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets(self) -> None:
        """Check that basic secret processing works."""
        with self._setup_secrets({
                'internal.yml': {'foo': {'.data': {'value': secrets.encrypt('qux')}}},
                'secrets.yml': {
                    'sec': {
                        '.data': {
                            'value': secrets.encrypt('baz'),
                            'key': secrets.encrypt('bar'),
                        },
                        '.meta': {
                            'foo': 'lis',
                        },
                    },
                },
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
            'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self.assertRaises(Exception, lambda: secrets.secret('foo'))
            self.assertEqual(secrets.secret('sec'), 'baz')
            self.assertEqual(secrets.secret('sec:key'), 'bar')
            self.assertEqual(secrets.secret('sec:'), {'value': 'baz', 'key': 'bar'})
            self.assertEqual(secrets.secret('sec#foo'), 'lis')
            self.assertEqual(secrets.secret('sec#'), {'foo': 'lis'})

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets_filtering(self) -> None:
        """Check that secrets filtering works."""
        cases = (
            ('empty', 'sec[]', ['a', 'b'],
             {'a': {'active': True}, 'b': {'active': False}}),
            ('simple', 'sec[active]', ['a'],
             {'a': {'active': True}, 'b': {'active': False}}),
            ('negated', 'sec[!active]', ['b'],
             {'a': {'active': True}, 'b': {'active': False}}),
            ('negated default', 'sec[!active]', ['b'],
             {'a': {'active': True}, 'b': {}}),
            ('multiple', 'sec[active]', ['a', 'b'],
             {'a': {'active': True}, 'b': {'active': True}, 'c': {}}),
            ('multiple conditions', 'sec[active,!revoked]', ['a'],
             {'a': {'active': True}, 'b': {'active': True, 'revoked': True}, 'c': {}}),
            ('meta', 'sec[active]#id', [1],
             {'a': {'active': True, 'id': 1}, 'b': {}}),
        )
        for description, key, expected, metas in cases:
            with self.subTest(description), self._setup_secrets({'secrets.yml': {
                f'sec/{k}': {'.data': {'value': secrets.encrypt(k)}, '.meta': v}
                for k, v in metas.items()
            }}) as directory, mock.patch.dict(os.environ, {
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }):
                self.assertEqual(secrets.secret(key), expected)

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_edit_secrets(self) -> None:
        """Check that secret can be edited."""
        encrypted = secrets.encrypt('foo')
        cases = (
            ('single', 'key', 'bar', 'bar',
             f'key:\n  .data:\n    value: |-\n      {encrypted}\n',
             f'key:\n  .data:\n    value: |-\n      {self.bar}\n'),
            ('empty', 'key', 'bar', 'bar',
             f'key:\n  .data:\n    value: |-\n      {encrypted}\n  .meta:\n\n    some: thing\n',
             f'key:\n  .data:\n    value: |-\n      {self.bar}\n  .meta:\n    some: thing\n'),
            ('after', 'key', 'bar', 'bar',
             f'key:\n  .data:\n    value: |-\n      {encrypted}\nsomething: |\n  else\n',
             f'key:\n  .data:\n    value: |-\n      {self.bar}\nsomething: |\n  else\n'),
            ('after empty',  'key', 'bar', 'bar',
             f'key:\n  .data:\n    value: |-\n      {encrypted}\n\nsomething: |\n  else\n',
             f'key:\n  .data:\n    value: |-\n      {self.bar}\n\nsomething: |\n  else\n'),
            ('before', 'key', 'bar', 'bar',
             f'something: |\n  else\nkey:\n  .data:\n    value: |-\n      {encrypted}\n',
             f'something: |\n  else\nkey:\n  .data:\n    value: |-\n      {self.bar}\n'),
            ('before empty', 'key', 'bar', 'bar',
             f'something: |\n  else\n\nkey:\n  .data:\n    value: |-\n      {encrypted}\n',
             f'something: |\n  else\n\nkey:\n  .data:\n    value: |-\n      {self.bar}\n'),
            ('missing', 'key', 'bar', 'bar',
             'something: |\n  else\n',
             f'something: |\n  else\nkey:\n  .data:\n    value: |-\n      {self.bar}\n'),
            ('no file', 'key', 'bar', 'bar',
             None,
             f'key:\n  .data:\n    value: |-\n      {self.bar}\n'),
            ('default', 'key', 'bar', 'bar',
             f'key:\n  .data:\n    value: |-\n      {encrypted}\n  .meta:\n    some: thing\n',
             f'key:\n  .data:\n    value: |-\n      {self.bar}\n  .meta:\n    some: thing\n'),
            ('key', 'key:bar', 'bar', 'foo',
             f'key:\n  .data:\n    value: |-\n      {encrypted}\n  .meta:\n    some: thing\n',
             'key:\n  .data:\n'
             f'    bar: |-\n      {self.bar}\n'
             f'    value: |-\n      {encrypted}\n'
             '  .meta:\n'
             '    some: thing\n'),
            ('meta', 'key#foo', 'qux', 'foo',
             f'key:\n  .data:\n    value: |-\n      {encrypted}\n  .meta:\n    some: thing\n',
             'key:\n  .data:\n'
             f'    value: |-\n      {encrypted}\n'
             '  .meta:\n'
             '    foo: qux\n'
             '    some: thing\n'),
            ('dict', 'key:', '{value: bar, baz: bar}', 'bar',
             f'key:\n  .data:\n    value: |-\n      {encrypted}\n  .meta:\n    some: thing\n',
             'key:\n  .data:\n'
             f'    baz: |-\n      {self.bar}\n'
             f'    value: |-\n      {self.bar}\n'
             '  .meta:\n'
             '    some: thing\n'),
            ('meta dict', 'key#', '{foo: bar, baz: bar}', 'foo',
             f'key:\n  .data:\n    value: |-\n      {encrypted}\n  .meta:\n    some: thing\n',
             'key:\n  .data:\n'
             f'    value: |-\n      {encrypted}\n'
             '  .meta:\n'
             '    baz: bar\n'
             '    foo: bar\n'),
        )
        for description, key, value, expected_value, before, after in cases:
            with self.subTest(description), self._setup_secrets({
                    'secrets.yml': before,
            } if before is not None else {}) as directory, mock.patch.dict(os.environ, {
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }), mock.patch('cki.deployment_tools.secrets.encrypt',
                           mock.Mock(return_value=self.bar)):
                secrets.edit(key, value)
                self.assertEqual(secrets.secret('key'), expected_value)
                self.assertEqual(pathlib.Path(f'{directory}/secrets.yml')
                                 .read_text(encoding='utf8'), after)

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets_unencrypted(self) -> None:
        """Check that uncrypted secret processing fails."""
        with self._setup_secrets({
                'internal.yml': {'foo': 'qux'},
                'secrets.yml': {'sec': 'ure'},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
            'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            self.assertRaises(Exception, lambda: secrets.secret('foo'))
            self.assertRaises(Exception, lambda: secrets.secret('sec'))

    def test_secrets_invalid(self) -> None:
        """Check that invalid secret files fail."""
        with self._setup_secrets({
                'internal.yml': "['bar', 'baz']",
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
        }):
            with self.assertRaises(Exception, msg='invalid'):
                secrets.variable('bar')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets_custom(self) -> None:
        """Check that custom secrets files work."""
        with self._setup_secrets({
                'custom-vars.yml': {'ure': 'qux'},
        }) as directory, mock.patch.dict(os.environ, {
            'CUSTOM_VARS_FILE': f'{directory}/custom-vars.yml',
            'CKI_VARS_NAMES': 'CUSTOM_VARS_FILE CUSTOM_VARS_FILE_2',
        }):
            self.assertEqual(secrets.variable('ure'), 'qux')

    @responses.activate
    @mock.patch.dict(os.environ, {
        'VAULT_ADDR': 'https://host',
        'ENVPASSWORD': 'unit-tests',
    })
    def test_secrets_vault(self) -> None:
        """Check that basic secret processing from HashiCorp Vault works."""
        responses.add(
            responses.GET, 'https://host/v1/apps/data/cki/foo',
            json={'data': {'data': {'value': 'bar', 'field': 'baz'}}})
        responses.add(
            responses.GET, 'https://host/v1/apps/data/cki/qux',
            json={'data': {'data': {'value': 'bar', 'field': 'baz'}}})

        data = (
            ('hv default key', 'foo', 'bar'),
            ('hv specified key', 'foo:field', 'baz'),
            ('hv hash', 'foo:', {'value': 'bar', 'field': 'baz'}),
            ('hv unknown key', 'foo:unknown-field', None),
            ('hv missing meta', 'qux', None),
            ('yaml fallback', 'abc', 'def'),
        )

        for description, location, expected in data:
            with self.subTest(description), self._setup_secrets({
                    'secrets.yml': {'foo': {}, 'abc': {'.data': {'value': secrets.encrypt('def')}}},
            }) as directory, mock.patch.dict(os.environ, {
                    'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }):
                if expected:
                    self.assertEqual(secrets.secret(location), expected)
                else:
                    self.assertRaises(Exception, secrets.secret, location)

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_encrypt(self) -> None:
        """Check that basic encryption works."""
        self.assertEqual(secrets.encrypt('bar', salt=False), '0eA1mjLawgSmRg9bpsdPDw==')
        self.assertTrue(secrets.encrypt('bar').startswith('U2FsdGVkX1'))

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_cli(self) -> None:
        """Check that CLI processing works."""
        cases = (
            ('var int', '_variable_cli', ['int'], '0\n'),
            ('var int json', '_variable_cli', ['int', '--json'], '0\n'),
            ('var string', '_variable_cli', ['str'], 'qux\n'),
            ('var string json', '_variable_cli', ['str', '--json'], '"qux"\n'),
            ('var list', '_variable_cli', ['list'], 'a\nb\n'),
            ('var list json', '_variable_cli', ['list', '--json'], '["a", "b"]\n'),
            ('secret', '_secret_cli', ['sec'], 'ure\n'),
            ('secret json', '_secret_cli', ['sec', '--json'], '"ure"\n'),
            ('secret list', '_secret_cli', ['sec[]'], 'ure\n'),
            ('secret list json', '_secret_cli', ['sec[]', '--json'], '["ure"]\n'),
            ('edit secret', '_edit_cli', ['sec', 'int'], ''),
            ('main var int', '_main', ['variable', 'int'], '0\n'),
            ('main var int json', '_main', ['variable', 'int', '--json'], '0\n'),
            ('main var string', '_main', ['variable', 'str'], 'qux\n'),
            ('main var string json', '_main', ['variable', 'str', '--json'], '"qux"\n'),
            ('main secret', '_main', ['secret', 'sec'], 'ure\n'),
            ('main edit secret', '_main', ['edit', 'sec', 'int'], ''),
        )
        for description, method, args, expected in cases:
            with self.subTest(description), self._setup_secrets({
                    'internal.yml': {'int': 0, 'str': 'qux', 'list': ['a', 'b']},
                    'secrets.yml': {'sec': {'.data': {'value': secrets.encrypt('ure')}}},
            }) as directory, mock.patch.dict(os.environ, {
                'CKI_VARS_FILE': f'{directory}/internal.yml',
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }):
                with contextlib.redirect_stdout(output := io.StringIO()):
                    getattr(secrets, method)(args)
                self.assertEqual(output.getvalue(), expected)
