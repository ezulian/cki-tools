[0KRunning with gitlab-runner 15.6.1 (133d7e76)[0;m
[0K  on wf-aws-aws-internal-a-dm-internal-general jstvQZqG[0;m
section_start:1671118644:resolve_secrets[0K[0K[36;1mResolving secrets[0;m[0;m
section_end:1671118644:resolve_secrets[0Ksection_start:1671118644:prepare_executor[0K[0K[36;1mPreparing the "docker+machine" executor[0;m[0;m
[0KUsing Docker executor with image quay.io/cki/builder-stream9:production ...[0;m
[0KAuthenticating with credentials from $DOCKER_AUTH_CONFIG[0;m
[0KPulling docker image quay.io/cki/builder-stream9:production ...[0;m
[0KUsing docker image sha256:12a3c39e4e9b0cb3cd06f2daacbd28886ab7e95b81763d9f2caed021d4f11c72 for quay.io/cki/builder-stream9:production with digest quay.io/cki/builder-stream9@sha256:03978f119436340062315211aff3c2bcae832d9e85d7d3f37b0eb067a44a7057 ...[0;m
section_end:1671118647:prepare_executor[0Ksection_start:1671118647:prepare_script[0K[0K[36;1mPreparing environment[0;m[0;m
Running on runner-jstvqzqg-project-18194050-concurrent-0 via runner-jstvqzqg-arr-cki.prod.general.1671117950-124f69df...
section_end:1671118648:prepare_script[0Ksection_start:1671118648:get_sources[0K[0K[36;1mGetting source from Git repository[0;m[0;m
[32;1mSkipping Git repository setup[0;m
[32;1mSkipping Git checkout[0;m
[32;1mSkipping Git submodules setup[0;m
section_end:1671118648:get_sources[0Ksection_start:1671118648:download_artifacts[0K[0K[36;1mDownloading artifacts[0;m[0;m
[32;1mDownloading artifacts for prepare builder (3480585301)...[0;m
Downloading artifacts from coordinator... ok      [0;m  id[0;m=3480585301 responseStatus[0;m=200 OK token[0;m=64_NRqVh
section_end:1671118650:download_artifacts[0Ksection_start:1671118650:step_script[0K[0K[36;1mExecuting "step_script" stage of the job script[0;m[0;m
[0KUsing docker image sha256:12a3c39e4e9b0cb3cd06f2daacbd28886ab7e95b81763d9f2caed021d4f11c72 for quay.io/cki/builder-stream9:production with digest quay.io/cki/builder-stream9@sha256:03978f119436340062315211aff3c2bcae832d9e85d7d3f37b0eb067a44a7057 ...[0;m
[32;1m$ # Create a precollapsed section to hide the usually uninteresting before-script # collapsed multi-line command[0;m
[0Ksection_start:1671118650:before_script[collapsed=true][0KExport needed functions and variables
[32;1m$ set -euo pipefail[0;m
[32;1m$ # Export general pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export AWS pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export KCIDB pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export functions used only by prepare # collapsed multi-line command[0;m
[32;1m$ # Export variables that we need to build kcidb schema # collapsed multi-line command[0;m
[32;1m$ # Set up the path for locally installed python executables and ccache. # collapsed multi-line command[0;m
🏠 Home directory is set: /tmp
📦 Container image:
  Image: builder-stream9
  Pipeline: https://gitlab.com/cki-project/containers/-/pipelines/719697322
  Job: https://gitlab.com/cki-project/containers/-/jobs/3458258213
  Merge Request: https://gitlab.com/cki-project/containers/-/merge_requests/440
  Commit: https://gitlab.com/cki-project/containers/-/commit/6f09099e1e933cb88e7b14ae75d2b1cb28428eea
  Commit title: Merge: Draft: Add Dependencies for unified kernels
  Commit timestamp: 2022-12-12T09:40:32+00:00
🏗️ API URLs:
  Pipeline: https://gitlab.com/api/v4/projects/18194050/pipelines/723700701
  Variables: https://gitlab.com/api/v4/projects/18194050/pipelines/723700701/variables
  Job: https://gitlab.com/api/v4/projects/18194050/jobs/3480585321
💻 Determining CPU and job count
  CPUs: 2
  job count: 3
[1;32m🗄️ Downloading artifacts from S3 bucket...[0m
Downloading artifacts for '723700701/prepare builder'
  Previous jobs: 3480585301
Downloading artifacts from 723700701/prepare builder/3480585301
Downloading artifacts
[1;32m📦 Extracting CKI pipeline software.[0m
[32;1m$ echo -e "\e[0Ksection_end:$(date +%s):before_script\r\e[0K"[0;m
[0Ksection_end:1671118659:before_script[0K
[32;1m$ kcidb_set_base_checkout_data[0;m
[32;1m$ # Download and clone repo, update, check out ref. # collapsed multi-line command[0;m
[1;33mPulling cached https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9.git...[0m
[1;33mIf this step fails, you have likely submitted the MR against an unsupported project.[0m
[1;33mIs https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9.git the correct target?[0m
warning: remote HEAD refers to nonexistent ref, unable to checkout.

[1;33mUpdating https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9.git...[0m
[1;32m📦 Retrieving merge request references[0m
From https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9
 * [new branch]                main       -> mr-target
From https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9
 * [new ref]                   refs/merge-requests/1794/head -> mr-feature
From https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9
 * [new ref]                   refs/merge-requests/1794/merge -> mr-merge
[1;32m📦 Validating merge commit[0m
  Target branch HEAD commit ID: 9cd5a5783c6529181ff799f22a08125da60b898c
  Feature branch HEAD commit ID: 6df7f95fe3e2d28fcc5752ab3f7f15d1dc5c1715
  Merge commit ID: 274d1239e4c400b4c53572776a5cc19b46c0c515
  Left side of merge commit ID: 9cd5a5783c6529181ff799f22a08125da60b898c
  Right side of merge commit ID: 6df7f95fe3e2d28fcc5752ab3f7f15d1dc5c1715
[1;32m📦 Git revision under test: merge result from merge-requests/1794/merge[0m
HEAD is now at 274d1239e4c4 Merge: Draft: fs: add mode_strip_sgid() helper
[1;32mSuccessfully checked out 274d1239e4c400b4c53572776a5cc19b46c0c515 from tree/MR.[0m
[32;1m$ # Get the tag of the current commit. # collapsed multi-line command[0;m
[1;32m🏷️ Generated short commit tag: -214.el9[0m
[32;1m$ # If applicable, merge any trees and apply patches # collapsed multi-line command[0;m
[32;1m$ # If this is a merge request, get a diff for targeted testing analysis # collapsed multi-line command[0;m
[32;1m$ kcidb_checkout set-bool valid true[0;m
[32;1m$ # Package the source code into a tarball. # collapsed multi-line command[0;m
[32;1m$ if ! is_true "${skip_build}"; then # collapsed multi-line command[0;m
[32;1m$ # Package the source code into a source RPM. # collapsed multi-line command[0;m
[1;33mBuilding source RPM...[0m
section_end:1671118883:step_script[0Ksection_start:1671118883:after_script[0K[0K[36;1mRunning after_script[0;m[0;m
[32;1mRunning after script...[0;m
[32;1m$ # Create a precollapsed section to hide the usually uninteresting after-script # collapsed multi-line command[0;m
[0Ksection_start:1671118884:after_script[collapsed=true][0KExport needed functions and variables
[32;1m$ # Export general pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export AWS pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export KCIDB pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export variables that we need to build kcidb schema # collapsed multi-line command[0;m
[32;1m$ echo -e "\e[0Ksection_end:$(date +%s):after_script\r\e[0K"[0;m
[0Ksection_end:1671118884:after_script[0K
[32;1m$ # Set log_url if there are merge logs # collapsed multi-line command[0;m
[32;1m$ # If srpm generation failed, save and print failure data # collapsed multi-line command[0;m
[1;31mSRPM generation failed, see the extracted errors below:[0m
[1;31m----------------------------------------------------------------------[0m
[32;1m$ # Upload artifacts in case of failure # collapsed multi-line command[0;m
🗄️ Deleting artifacts that should not be archived...
🗄️ Generating artifact meta data...
🗄️ Uploading artifacts to S3 bucket...
Uploading kcidb_all.json file
Completed 3.7 KiB/3.7 KiB (29.6 KiB/s) with 1 file(s) remainingupload: ./kcidb_all.json to s3://arr-cki-prod-trusted-artifacts/trusted-artifacts/723700701/merge/3480585321/kcidb_all.json
Uploading artifacts
🗄️ Generating and uploading static artifact directory list...
Completed 866 Bytes/866 Bytes (5.0 KiB/s) with 1 file(s) remainingupload: ./index.html to s3://arr-cki-prod-trusted-artifacts/trusted-artifacts/723700701/merge/3480585321/index.html
Removing artifacts to prevent GitLab from archiving them
[32;1m$ # Display a couple of relevant links # collapsed multi-line command[0;m
This pipeline uses artifacts stored on S3:
[1;33m https://s3.amazonaws.com/arr-cki-prod-trusted-artifacts/index.html?prefix=trusted-artifacts/723700701/merge/3480585321/[0m
More information about this pipeline can be found in DataWarehouse:
[1;33m  https://datawarehouse.cki-project.org/kcidb/checkouts/redhat:723700701[0m
Check out CKI documentation if you have questions about next steps:
[1;33m  https://cki-project.org/l/devel-faq[0m
section_end:1671118914:after_script[0Ksection_start:1671118914:upload_artifacts_on_failure[0K[0K[36;1mUploading artifacts for failed job[0;m[0;m
[32;1mUploading artifacts...[0;m
artifacts: found 1 matching files and directories [0;m 
artifacts-meta.json: found 1 matching files and directories[0;m 
kcidb_all.json: found 1 matching files and directories[0;m 
Uploading artifacts as "archive" to coordinator... 201 Created[0;m  id[0;m=3480585321 responseStatus[0;m=201 Created token[0;m=64_NRqVh
section_end:1671118916:upload_artifacts_on_failure[0Ksection_start:1671118916:cleanup_file_variables[0K[0K[36;1mCleaning up project directory and file based variables[0;m[0;m
section_end:1671118917:cleanup_file_variables[0K[31;1mERROR: Job failed: exit code 255
[0;m
