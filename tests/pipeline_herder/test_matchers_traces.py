"""Test matchers against real world traces."""
from importlib import resources
import unittest
from unittest import mock

import responses

from cki_tools.pipeline_herder import main
from cki_tools.pipeline_herder import matchers
from cki_tools.pipeline_herder import settings
from tests.pipeline_herder.assets import traces


@mock.patch.object(settings, 'HERDER_ACTION', 'retry')
@mock.patch('cki_lib.misc.is_production', mock.Mock(return_value=True))
class TestMatchersWithTraces(unittest.TestCase):
    """Test matchers against regexex."""

    @staticmethod
    def _get_matcher_by_name(name):
        """Return matcher from matchers.MATCHERS by name."""
        return next((m for m in matchers.MATCHERS if m.name == name))

    @staticmethod
    def mock_responses(trace_content, name):
        """Mock GitLab requests."""
        base_url = 'https://host/api/v4/projects/project'

        mocks = [
            (f'{base_url}', {"id": "project", "path_with_namespace": "project"}),
            (f'{base_url}/pipelines/524445', {"id": 524445}),
            (f'{base_url}/pipelines/524445/variables', []),
            (f'{base_url}/pipelines/524445/jobs?include_retried=True',
             [{"id": 764828, "name": f"{name} x86_64", "pipeline": {"id": 524445}}]),
            (f'{base_url}/jobs/764828',
             {"id": 764828, "status": "failed", "stage": name, "name": f"{name} x86_64",
              "pipeline": {"id": 524445, "project_id": "project"},
              "web_url": "https://host/project/-/jobs/764828"}),
        ]

        for url, payload in mocks:
            responses.add(responses.GET, url, json=payload)

        responses.add(responses.GET, f'{base_url}/jobs/764828/trace', body=trace_content)

    @responses.activate
    @mock.patch('cki_tools.pipeline_herder.main.submit_retry')
    @mock.patch('cki_tools.pipeline_herder.utils.notify')
    @mock.patch('cki_tools.pipeline_herder.main.notify_finished', mock.Mock())
    def _test(
        self,
        matcher_name,
        trace_filename,
        notify=None,
        submit_retry=None,
        *,
        retry=True,
        name='build',
    ) -> None:
        """Test that matcher_name is found in trace_filename."""
        my_resources = resources.files(traces)
        trace_content = (
            my_resources / trace_filename
        ).read_text(
            encoding='utf-8',
            errors='strict'
        )
        self.mock_responses(trace_content, name)

        main.process_job('https://host/project/-/jobs/764828')

        if retry:
            submit_retry.assert_called_once_with(
                mock.ANY, self._get_matcher_by_name(matcher_name), mock.ANY
            )
        notify.assert_called()

    def test_artifacts_error_forbidden(self):
        """Test artifacts-error with 403 forbidden error."""
        self._test('artifacts-error', 'artifact_error_forbidden.txt')

    def test_artifacts_error_forbidden_upload(self):
        """Test artifacts-error with 403 forbidden error on upload."""
        self._test('artifacts-error', 'artifact_error_forbidden_upload.txt')

    def test_aws_credentials(self):
        """Test AWS credentials location error."""
        self._test('aws-credentials', 'aws_credentials.txt')

    def test_inotify_limit_reached(self):
        """Test inotify matcher."""
        self._test('inotify-limit-reached', 'inotify_limit_reached.txt',
                   name='test', retry=False)

    def test_code255(self):
        """Test exit code 255 error."""
        self._test('code255', 'code255.txt')

    def test_mainline_build_infra_issue(self):
        """Test mainline build infra issue."""
        self._test('mainline-build-infra-issue', 'mainline_build_infra_issue.txt',
                   retry=False)

    def test_no_space_left_on_device(self):
        """Test volume space issue."""
        self._test('no-space-left-on-device', 'no_space_left_on_device.txt',
                   retry=False)

    def test_krb_ticket_not_yet_valid(self):
        """Test krb clock skew issue."""
        self._test('krb-ticket-not-yet-valid', 'krb_ticket_not_yet_valid.txt')

    def test_unsupported_cpu_model(self):
        """Test unsupported cpu model issue."""
        self._test('unsupported-cpu-model', 'unsupported_cpu_model.txt')

    def test_datawarehouse_down(self):
        """Test datawarehouse down issue."""
        self._test('datawarehouse-down', 'datawarehouse_down_502.txt', name='check-kernel-results')
