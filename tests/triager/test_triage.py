"""Test checkers.triage()."""
import unittest
from unittest import mock
from unittest.mock import sentinel

from cki_lib import misc

from cki.triager import checkers
from cki.triager import compiledregex
from cki.triager import dwobject

from ..utils import tear_down_registry

tear_down_registry()


class TestTriage(unittest.TestCase):
    """Test checkers.triage()."""

    def test_triage_not_needed(self) -> None:
        """Test not triaging checkers.triage()."""
        cases = (
            ('empty checkout', 'checkout', {}, checkers.TriageStatus.INCOMPLETE),
            ('none checkout', 'checkout', {'valid': None}, checkers.TriageStatus.INCOMPLETE),
            ('invalid checkout', 'checkout', {'valid': False}, checkers.TriageStatus.SUCCESS),
            ('no output invalid checkout', 'checkout',
             {'valid': False, 'output_files': []}, checkers.TriageStatus.SUCCESS),
            ('empty log invalid checkout', 'checkout',
             {'valid': False, 'log_url': None}, checkers.TriageStatus.SUCCESS),
            ('valid checkout', 'checkout', {'valid': True}, checkers.TriageStatus.NOT_NEEDED),
            ('empty build', 'build', {}, checkers.TriageStatus.INCOMPLETE),
            ('none build', 'build', {'valid': None}, checkers.TriageStatus.INCOMPLETE),
            ('invalid build', 'build', {'valid': False}, checkers.TriageStatus.SUCCESS),
            ('no output invalid build', 'build',
             {'valid': False, 'output_files': []}, checkers.TriageStatus.SUCCESS),
            ('empty log invalid build', 'build',
             {'valid': False, 'log_url': None}, checkers.TriageStatus.SUCCESS),
            ('valid build', 'build', {'valid': True}, checkers.TriageStatus.NOT_NEEDED),
            ('empty test', 'test', {}, checkers.TriageStatus.INCOMPLETE),
            ('none test', 'test', {'status': None}, checkers.TriageStatus.INCOMPLETE),
            ('fail test', 'test', {'status': 'FAIL'}, checkers.TriageStatus.SUCCESS),
            ('no output fail test', 'test',
             {'status': 'FAIL', 'output_files': []}, checkers.TriageStatus.SUCCESS),
            ('empty log fail test', 'test',
             {'status': 'FAIL', 'log_url': None}, checkers.TriageStatus.SUCCESS),
            ('error test', 'test', {'status': 'ERROR'}, checkers.TriageStatus.SUCCESS),
            ('no output error test', 'test',
             {'status': 'ERROR', 'output_files': []}, checkers.TriageStatus.SUCCESS),
            ('empty log error test', 'test',
             {'status': 'ERROR', 'log_url': None}, checkers.TriageStatus.SUCCESS),
            ('miss test', 'test', {'status': 'MISS'}, checkers.TriageStatus.NOT_NEEDED),
            ('pass test', 'test', {'status': 'PASS'}, checkers.TriageStatus.NOT_NEEDED),
            ('done test', 'test', {'status': 'DONE'}, checkers.TriageStatus.NOT_NEEDED),
            ('skip test', 'test', {'status': 'SKIP'}, checkers.TriageStatus.NOT_NEEDED),
            ('fail subtest', 'test', {'status': 'PASS', 'misc': {
             'results': [{'status': 'FAIL'}, ]}}, checkers.TriageStatus.SUCCESS),
            ('error subtest', 'test', {'status': 'PASS', 'misc': {
             'results': [{'status': 'ERROR'}, ]}}, checkers.TriageStatus.SUCCESS),
            ('miss subtest', 'test', {'status': 'PASS', 'misc': {
             'results': [{'status': 'MISS'}, ]}}, checkers.TriageStatus.NOT_NEEDED),
            ('pass subtest', 'test', {'status': 'PASS', 'misc': {
             'results': [{'status': 'PASS'}, ]}}, checkers.TriageStatus.NOT_NEEDED),
            ('done subtest', 'test', {'status': 'PASS', 'misc': {
             'results': [{'status': 'DONE'}, ]}}, checkers.TriageStatus.NOT_NEEDED),
            ('skip subtest', 'test', {'status': 'PASS', 'misc': {
             'results': [{'status': 'SKIP'}, ]}}, checkers.TriageStatus.NOT_NEEDED),
        )
        for description, object_type, object_data, expected_status in cases:
            with self.subTest(description), \
                    mock.patch('cki.triager.compiledregex.get_compiled_issueregexes',
                               mock.Mock(return_value=[sentinel.rx1, sentinel.rx2])), \
                    mock.patch('cki.triager.checkers.match') as match:
                dw_obj = dwobject.from_attrs(object_type, attrs=object_data)
                result = checkers.triage(dw_obj, sentinel.regexes)
                compiledregex.get_compiled_issueregexes.assert_not_called()
                match.assert_not_called()
                self.assertEqual(result, checkers.TriageResult(expected_status, []))

    def test_triage(self) -> None:
        """Test triaging via checkers.triage()."""
        cases = (
            ('checkout', 'checkout',
             {'valid': False, 'output_files': [{'name': 'n', 'url': '0'}]}, ['n']),
            ('log checkout', 'checkout', {'valid': False, 'log_url': '0'}, ['merge.log']),
            ('build', 'build',
             {'valid': False, 'output_files': [{'name': 'n', 'url': '0'}]}, ['n']),
            ('log build', 'build', {'valid': False, 'log_url': '0'}, ['build.log']),
            ('fail test', 'test',
             {'status': 'FAIL', 'output_files': [{'name': 'n', 'url': '0'}]}, ['n']),
            ('log fail test', 'test', {'status': 'FAIL', 'log_url': '0'}, ['test.log']),
            ('error test', 'test',
             {'status': 'ERROR', 'output_files': [{'name': 'n', 'url': '0'}]}, ['n']),
            ('log error test', 'test', {'status': 'ERROR', 'log_url': '0'}, ['test.log']),
        )
        for description, object_type, object_data, expected_names in cases:
            with self.subTest(description), \
                    mock.patch('cki.triager.compiledregex.get_compiled_issueregexes',
                               mock.Mock(return_value=[sentinel.rx1, sentinel.rx2])), \
                    mock.patch('cki.triager.checkers.match',
                               return_value=checkers.MatchStatus.FULL_MATCH) as match:
                dw_obj = dwobject.from_attrs(object_type, attrs=object_data)
                result = checkers.triage(dw_obj, sentinel.regexes)
                compiledregex.get_compiled_issueregexes.assert_called_with(sentinel.regexes)
                match.assert_has_calls(list(misc.flattened([[
                    mock.call(checkers.LogFile(dw_obj=dw_obj, name=n, url=str(i)), sentinel.rx1),
                    mock.call(checkers.LogFile(dw_obj=dw_obj, name=n, url=str(i)), sentinel.rx2),
                ] for i, n in enumerate(expected_names)])))
                self.assertEqual(result, checkers.TriageResult(
                    checkers.TriageStatus.SUCCESS, list(misc.flattened([[
                        checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                            checkers.LogFile(dw_obj=dw_obj, name=n, url=str(i)),
                                            sentinel.rx1),
                        checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                            checkers.LogFile(dw_obj=dw_obj, name=n, url=str(i)),
                                            sentinel.rx2),
                    ] for i, n in enumerate(expected_names)]))
                ))

    def test_triage_testresults(self) -> None:
        """Test testresult triaging via checkers.triage()."""
        cases = (
            ('fail test', 'test',
             {'status': 'FAIL', 'log_url': '0', 'output_files': [{'name': 'n', 'url': '1'}]},
             ['test.log', 'n'], []),
            ('fail test but no failed subtest', 'test',
             {'status': 'FAIL', 'log_url': '0', 'output_files': [{'name': 'n', 'url': '1'}],
              'misc': {'results': [
                  {'status': 'PASS', 'output_files': [{'name': 'r', 'url': '2'}]},
              ]}},
             ['test.log', 'n'], []),
            ('fail test and failed subtest', 'test',
             {'status': 'FAIL', 'log_url': '0', 'output_files': [{'name': 'n', 'url': '1'}],
              'misc': {'results': [
                  {'status': 'FAIL', 'output_files': [{'name': 'r', 'url': '2'}]},
              ]}},
             ['test.log', 'n'], ['test.log', 'n', 'r']),
            ('success test and failed subtest', 'test',
             {'status': 'PASS', 'log_url': '0', 'output_files': [{'name': 'n', 'url': '1'}],
              'misc': {'results': [
                  {'status': 'FAIL', 'output_files': [{'name': 'r', 'url': '2'}]},
              ]}},
             ['test.log', 'n'], ['test.log', 'n', 'r']),
        )
        for description, object_type, object_data, expected_names, \
                expected_testresult_names in cases:
            with self.subTest(description), \
                    mock.patch('cki.triager.compiledregex.get_compiled_issueregexes',
                               mock.Mock(return_value=[sentinel.rx1, sentinel.rx2])), \
                    mock.patch('cki.triager.checkers.match',
                               return_value=checkers.MatchStatus.FULL_MATCH) as match:
                dw_obj = dwobject.from_attrs(object_type, attrs=object_data)
                result = checkers.triage(dw_obj, sentinel.regexes)
                compiledregex.get_compiled_issueregexes.assert_called_with(sentinel.regexes)
                match.assert_has_calls(list(misc.flattened([[
                    mock.call(checkers.LogFile(dw_obj=dw_obj, name=n, url=str(i)), sentinel.rx1),
                    mock.call(checkers.LogFile(dw_obj=dw_obj, name=n, url=str(i)), sentinel.rx2),
                ] for i, n in enumerate(expected_names)]+[[
                    mock.call(checkers.LogFile(dw_obj=mock.ANY, name=n, url=str(i)), sentinel.rx1),
                    mock.call(checkers.LogFile(dw_obj=mock.ANY, name=n, url=str(i)), sentinel.rx2),
                ] for i, n in enumerate(expected_testresult_names)])))
                self.assertEqual(result.status, checkers.TriageStatus.SUCCESS)
                self.assertEqual(result.matches, list(misc.flattened([[
                    checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                        checkers.LogFile(dw_obj=dw_obj, name=n, url=str(i)),
                                        sentinel.rx1),
                    checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                        checkers.LogFile(dw_obj=dw_obj, name=n, url=str(i)),
                                        sentinel.rx2),
                ] for i, n in enumerate(expected_names)] + [[
                    checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                        checkers.LogFile(dw_obj=mock.ANY, name=n, url=str(i)),
                                        sentinel.rx1),
                    checkers.RegexMatch(checkers.MatchStatus.FULL_MATCH,
                                        checkers.LogFile(dw_obj=mock.ANY, name=n, url=str(i)),
                                        sentinel.rx2),
                ] for i, n in enumerate(expected_testresult_names)])))
