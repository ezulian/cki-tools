"""Test checkers."""
import unittest
from unittest import mock

from datawarehouse import Datawarehouse
import responses

from cki.triager import cache
from cki.triager import checkers
from cki.triager import compiledregex
from cki.triager import dwobject

from ..utils import mock_attrs

MOCK_REGEXES = [
    {
        "id": 2,
        "issue": {
            "id": 88,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "Bug description",
            "ticket_url": "https://bug.link",
            "resolved": False,
            "generic": False
        },
        "text_match": "bnx2x .* Direct firmware load for",
        "file_name_match": None,
        "test_name_match": None
    },
    {
        "id": 15,
        "issue": {
            "id": 126,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "BUG: clone.*failing after kernel commit",
            "ticket_url": "http://url.com",
            "resolved": False,
            "generic": False
        },
        "text_match": "tag=kcmp03.*FAIL: clone",
        "file_name_match": "syscalls.fail.log",
        "test_name_match": "LTP"
    }
]


class CheckersTest(unittest.TestCase):
    """Test checkers."""

    def setUp(self):
        """Set up tests."""
        checkers.download.cache_clear()
        compiledregex._get_compiled_issueregexes.cache_clear()  # pylint: disable=protected-access
        cache.get_build.cache_clear()
        cache.get_checkout.cache_clear()
        cache.get_issueregexes.cache_clear()

        responses.get('http://datawarehouse/api/1/kcidb/builds/redhat:952371',
                      json=mock_attrs(checkout_id=1))
        responses.get('http://datawarehouse/api/1/kcidb/checkouts/1', json=mock_attrs())

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
    def test_triage(self):
        """Test triage."""
        responses.add(responses.HEAD, url='https://logs/foobar', content_type='not/plain-text')
        responses.add(responses.HEAD, url='https://logs/console.log', content_type='not/plain-text')
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': MOCK_REGEXES})

        test = dwobject.from_attrs('test', {
            'id': 'redhat:113990600', "origin": "redhat", "misc": {"iid": 1},
            'status': 'FAIL',
            'build_id': 'redhat:952371',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/foobar', 'name': '8704397_aarch64_2_foobar'},
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
            ]
        })

        issues = checkers.triage(test, [])
        self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
        self.assertEqual(len(issues.matches), 2)
        self.assertEqual(issues.matches[0].status, checkers.MatchStatus.NO_MATCH)
        self.assertEqual(issues.matches[0].regex.issue_id, 88)
        self.assertEqual(issues.matches[1].status, checkers.MatchStatus.FULL_MATCH)
        self.assertEqual(issues.matches[1].regex.issue_id, 88)

        # Logs have no info.
        responses.replace(responses.GET, url='https://logs/console.log',
                          body=b'2019-11-17 07:15:09,105   ')
        issues = checkers.triage(test, [])
        self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
        self.assertEqual(len(issues.matches), 2)
        self.assertEqual(issues.matches[0].status, checkers.MatchStatus.NO_MATCH)
        self.assertEqual(issues.matches[0].regex.issue_id, 88)
        self.assertEqual(issues.matches[1].status, checkers.MatchStatus.NO_MATCH)
        self.assertEqual(issues.matches[1].regex.issue_id, 88)

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
    def test_logfile_redirects(self):
        """Test triage works as expected on HTTP redirects."""
        redirecting_url = "https://logs/foobar"
        file_url = "https://real-logs/foobar"

        responses.add(responses.HEAD, redirecting_url, status=301, headers={"Location": file_url})
        responses.add(responses.GET, redirecting_url, status=301, headers={"Location": file_url})

        responses.add(responses.HEAD, url=file_url, content_type='text/plain')
        responses.add(responses.GET, url=file_url, body=(
            b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for bfq_bfqq_move '))
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': MOCK_REGEXES})
        test = dwobject.from_attrs('test', {
            'id': 'redhat:113990600', "origin": "redhat", "misc": {"iid": 1},
            'status': 'FAIL',
            'build_id': 'redhat:952371',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [{'url': redirecting_url, 'name': 'foobar'}]
        })

        with self.subTest("Log content-type is valid ()= text/plain)"):
            issues = checkers.triage(test, [])
            responses.assert_call_count(redirecting_url, 2)
            responses.assert_call_count(file_url, 2)
            self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
            self.assertEqual(len(issues.matches), 1)
            self.assertEqual(issues.matches[0].status, checkers.MatchStatus.FULL_MATCH)
            self.assertEqual(issues.matches[0].regex.issue_id, 88)

        responses.calls.reset()

        self.assertEqual(list(responses.calls), [])

        with self.subTest("Log content-type is invalid (!= text/plain)"
                          " and content-length is small enough (<= MAX_CONTENT_LENGTH)"):
            bad_content_type = 'not/plain'
            responses.replace(responses.HEAD, url=file_url, content_type=bad_content_type,
                              headers={"Content-Length": '74'})
            checkers.download.cache_clear()

            issues = checkers.triage(test, [])

            responses.assert_call_count(redirecting_url, 2)
            responses.assert_call_count(file_url, 2)
            self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
            self.assertEqual(len(issues.matches), 1)
            self.assertEqual(issues.matches[0].status, checkers.MatchStatus.FULL_MATCH)
            self.assertEqual(issues.matches[0].regex.issue_id, 88)

        responses.calls.reset()

        with self.subTest("Log content-type is invalid (!= text/plain)"
                          " and content-length is too big (> MAX_CONTENT_LENGTH)"):
            bad_content_type = 'not/plain'
            bad_content_length = 500000001
            responses.replace(responses.HEAD, url=file_url, content_type=bad_content_type,
                              headers={"Content-Length": str(bad_content_length)})
            checkers.download.cache_clear()

            with mock.patch.object(checkers.LOGGER, "warning") as mocked_logger:
                issues = checkers.triage(test, [])

            responses.assert_call_count(redirecting_url, 1)  # NOTE: shouldn't hit the GET
            responses.assert_call_count(file_url, 1)
            self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
            self.assertEqual(len(issues.matches), 1)
            self.assertEqual(issues.matches[0].status, checkers.MatchStatus.NO_MATCH)
            self.assertEqual(issues.matches[0].regex.issue_id, 88)

            mocked_logger.assert_called_once_with(
                "Rejecting to read file %r with content_type=%r and content_length=%r",
                redirecting_url, bad_content_type, bad_content_length)

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
    def test_multiple_matches(self):
        """Test triage. More than one regex matches."""
        responses.add(responses.HEAD, url='https://logs/console.log')
        responses.add(responses.HEAD, url='https://logs/syscalls.fail.log')
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, url='https://logs/syscalls.fail.log',
                      body=(b'[   41.451946] tag=kcmp03 error error FAIL: clone'))

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': MOCK_REGEXES})
        test = dwobject.from_attrs('test', {
            'id': 'redhat:113990600', "origin": "redhat", "misc": {"iid": 1},
            'status': 'FAIL',
            'build_id': 'redhat:952371',
            'path': 'ltp',
            'comment': 'LTP',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
                {'url': 'https://logs/syscalls.fail.log',
                 'name': '8704397_aarch64_2_syscalls.fail.log'},
            ]
        })

        issues = checkers.triage(test, [])
        self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
        self.assertEqual(len(issues.matches), 3)
        self.assertEqual(issues.matches[0].status, checkers.MatchStatus.FULL_MATCH)
        self.assertEqual(issues.matches[0].regex.issue_id, 88)
        self.assertEqual(issues.matches[1].status, checkers.MatchStatus.NO_MATCH)
        self.assertEqual(issues.matches[1].regex.issue_id, 88)
        self.assertEqual(issues.matches[2].status, checkers.MatchStatus.FULL_MATCH)
        self.assertEqual(issues.matches[2].regex.issue_id, 126)

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
    def test_lazy(self):
        """Test triage doesnt download file if not necessary."""
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [MOCK_REGEXES[1]]})  # This regex has test constraints
        test = dwobject.from_attrs('test', {
            'id': 'redhat:113990600', "origin": "redhat", "misc": {"iid": 1},
            'status': 'FAIL',
            'build_id': 'redhat:952371',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/foobar', 'name': '8704397_aarch64_2_foobar'},
            ]
        })

        # check does not fail to find the mocked URL
        issues = checkers.triage(test, [])
        self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
        self.assertEqual(len(issues.matches), 0)
